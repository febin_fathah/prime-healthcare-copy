//
//  FavoriteDoctor+CoreDataProperties.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FavoriteDoctor+CoreDataProperties.h"

@implementation FavoriteDoctor (CoreDataProperties)

@dynamic doctorName;
@dynamic doctorId;
@dynamic regNumber;
@dynamic clinicId;
@dynamic clinicName;
@dynamic departmentId;
@dynamic discriptionText;
@dynamic doctorPhoto;
@dynamic email;
@dynamic phone;
@dynamic qualification;
@dynamic favoriteAdded;
@dynamic departmentName;

@end
