//
//  UserProfile.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "WebService.h"
#import "USLUserProfile.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserProfile : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (void)saveUserDetails:(USLUserProfile *)profile;
+ (void)deleteUserDetails;
+ (UserProfile *)getUserProfile;


@end

NS_ASSUME_NONNULL_END

#import "UserProfile+CoreDataProperties.h"
