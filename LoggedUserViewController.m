//
//  LoggedUserViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/30/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "LoggedUserViewController.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"
#import "CMEArrayOfServiceResult.h"
#import "CMEServiceResult.h"
#import "CCUBasicHttpBinding_IPrimeAppService.h"
#import "CCUServiceResult.h"
#import "CCUArrayOfServiceResult.h"
#import "MGTBasicHttpBinding_IPrimeAppService.h"
#import "MGTLoginDetails.h"
#import "MGTArrayOfLoginDetails.h"
#import "Header.h"
#define SUCCESS_RESPONCE @"200"

@interface LoggedUserViewController ()<CMESoapServiceResponse,CCUSoapServiceResponse,MGTSoapServiceResponse>

@end

@implementation LoggedUserViewController{

    NSString *regID;
    NSString *deviceID;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    regID=[PUtilities getRegId];
   deviceID=[[NSUserDefaults standardUserDefaults]valueForKey:@"DEVICE_ID"];

    [self setupView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupView{
    _name.text=[PUtilities getName];

    self.view.layer.cornerRadius = 3.0;
    self.view.clipsToBounds = YES;
    _submitBTN.layer.cornerRadius = 3.0;
    _submitBTN.clipsToBounds = YES;
    _profImage.layer.cornerRadius=_profImage.frame.size.height/2;
    _profImage.clipsToBounds=YES;
    
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_profImage.frame.size.width), (_profImage.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:_profImage.layer.cornerRadius];
    [borderLayer setBorderWidth:1];
    [borderLayer setBorderColor:[[UIColor orangeColor] CGColor]];
    [_profImage.layer addSublayer:borderLayer];
}


#pragma mark- button action
- (IBAction)submitAction:(id)sender {
    
    if (![_pwdTextFeild.text isEqualToString:@""]) {
        NSLog(@"%@",[PUtilities getpassword]);
        if (![_pwdTextFeild.text isEqualToString:[PUtilities getpassword]]) {
            [self showAlert:@"Alert" message:@"Incorrect password"];
 
        }
        else{
            [SVProgressHUD showWithStatus:@"authenticating..."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                MGTBasicHttpBinding_IPrimeAppService *service=[MGTBasicHttpBinding_IPrimeAppService new];
                [service GetDeviceInfoAsync:regID Password:_pwdTextFeild.text TransType:@"GET_DEVICE_INFO" __target:self];
            });
        }
    }
    else{
        
        [self showAlert:@"Alert" message:@"password field empty"];
    }
   
    
    
}
- (IBAction)forgotPassword:(id)sender {
    
    if ([self isInternetConnection]) {
        [SVProgressHUD showWithStatus:@"sending OTP..."];
        CMEBasicHttpBinding_IPrimeAppService *service=[CMEBasicHttpBinding_IPrimeAppService new];
        [service ForgotPasswordAsync:[NSString stringWithFormat:@"RG%@",[PUtilities getMobile]] OTPPassword:@"" TransType:@"FORGOT_PASSWORD" __target:self];
    }
    else{
        [self showNoInternetAlert];
    }

}
- (IBAction)skipBTN:(id)sender {
    [_delegate didClickSkipButton];
}

#pragma mark- SOAP responce
-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        NSLog(@"%@",value);
        if ([methodName isEqualToString:@"ForgotPassword"]) {
            CMEArrayOfServiceResult *result=(CMEArrayOfServiceResult *)value;
            CMEServiceResult *respoce=[CMEServiceResult new];
            respoce=[result.items objectAtIndex:0];
            if ([respoce.result isEqual:SUCCESS_RESPONCE]) {
                NSLog(@"%@",respoce.otpPassword);
                [_delegate recivedOTP:respoce.otpPassword withRegisterNumber:[NSString stringWithFormat:@"RG%@",[PUtilities getRegId]] fromLoggedUserViewController:self];
            }
            else{
                [self showAlert:@"Alert" message:@"Sending Failed,please try again"];
            }
        }
        
        
        if ([methodName isEqualToString:@"UpdatePushToken"]) {
            
            NSLog(@"%@",methodName);
            
            CCUArrayOfServiceResult *responce=(CCUArrayOfServiceResult *)value;
            CCUServiceResult *result=(CCUServiceResult *)[responce.items objectAtIndex:0];
            
            if ([result.result isEqualToString:SUCCESS_RESPONCE]) {
                
                
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLogged"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOAD_SIDE_MENU" object:nil];
                [self showAlert:@"Success" message:@"User logged in Successfully"];
                [_delegate loginCompletedSuccessfully];
            }
            else{
                [self showAlert:@"Failed" message:@"Login failed,please try again"];
            }
        }
        
        if ([methodName isEqualToString:@"GetDeviceInfo"]) {
            NSLog(@"%@",methodName);
            MGTArrayOfLoginDetails *result=(MGTArrayOfLoginDetails *)value;
            if (result.items.count!=0) {
                MGTLoginDetails *loginUser=(MGTLoginDetails *)[result.items objectAtIndex:0];
                
                if([loginUser.result isEqualToString:@"200"]){
                    
                    
                    //device Id will be compared with the current device Id locally, and if same, login and call UpdatePushToken service

                    if ([deviceID isEqualToString:loginUser.DeviceId]) {
                        
                        [self updatePushTokenServiceWithUserId:regID];
                        
                    }
                    else{
//                        According to new change update the device as new device for now
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"warning"message:@"Please confirm to make this is your default device.Any record of previous user will be cleared" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                            NSString *rgNo=[NSString stringWithFormat:@"RG%@",regID];
                            
                            [self updatePushTokenServiceWithUserId:regID];
                            
                            
                        }];
                        UIAlertAction* cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            
                        }];
                        [alertController addAction:ok];
                        [alertController addAction:cancel];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        
                        
//                    }
                    
                    
                        
                       /* UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Login Restricted"message:@"your login restricted on this device" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            [_delegate didClickSkipButton];
                            
                        }];
                       
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];*/
                        
                        
                        
                    }
                    
                }
                else{
                    
                    [self showAlert:@"Login Failed" message:loginUser.message];
                }
                
            }
            else{
                [self showAlert:@"Alert" message:@"login failed, please try again"];
                
            }
            
            
            
        }
        

        
    });
}
-(void)onError:(NSError *)error{
    
    NSLog(@"");
    [SVProgressHUD dismiss];
    [self showAlert:@"Error!" message:[error localizedDescription]];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)updatePushTokenServiceWithUserId:(NSString *)regid{
    [self.view endEditing:YES];
    [SVProgressHUD showWithStatus:@"Login..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CCUBasicHttpBinding_IPrimeAppService *pushTokenService=[CCUBasicHttpBinding_IPrimeAppService new];
        
        NSString *fcmToken=[[NSUserDefaults standardUserDefaults]valueForKey:@"FCM_TOKEN"];
        //uuuid
        deviceID = [deviceID stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [pushTokenService UpdatePushTokenAsync:deviceID FCMToken:fcmToken DeviceType:@"1" RGNo:regid TransType:@"UPDATE_PUSH_TOKEN " __target:self];
        
        
    });
    
    
}
@end
