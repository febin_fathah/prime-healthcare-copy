//
//  defaults.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef defaults_h
#define defaults_h
#define NAV_TINT_COLOR_HEX @"F67A20"
#define DOCTORE_PHONE @"AF9CCA"
#define DOCTORE_EMAIL @"EFBA50"

#define BORDERCOLOR  [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1]
#define BACKGROUNDCOLOR [UIColor colorWithRed:0.9647 green:0.9647 blue:0.9647 alpha:1]


// color
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#endif /* defaults_h */
