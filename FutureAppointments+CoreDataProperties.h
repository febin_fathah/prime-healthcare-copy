//
//  FutureAppointments+CoreDataProperties.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FutureAppointments.h"

NS_ASSUME_NONNULL_BEGIN

@interface FutureAppointments (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *appointmentDate;
@property (nullable, nonatomic, retain) NSString *branchName;
@property (nullable, nonatomic, retain) NSString *doctorName;
@property (nullable, nonatomic, retain) NSString *doctorPhoto;
@property (nullable, nonatomic, retain) NSString *qualification;
@property (nullable, nonatomic, retain) NSString *speciality;

@end

NS_ASSUME_NONNULL_END
