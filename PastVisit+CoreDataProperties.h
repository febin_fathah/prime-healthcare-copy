//
//  PastVisit+CoreDataProperties.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PastVisit.h"

NS_ASSUME_NONNULL_BEGIN

@interface PastVisit (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *branchCode;
@property (nullable, nonatomic, retain) NSString *branchId;
@property (nullable, nonatomic, retain) NSString *branchName;
@property (nullable, nonatomic, retain) NSString *doctorId;
@property (nullable, nonatomic, retain) NSString *doctorName;
@property (nullable, nonatomic, retain) NSString *doctorPhoto;
@property (nullable, nonatomic, retain) NSString *qualification;
@property (nullable, nonatomic, retain) NSString *speciality;
@property (nullable, nonatomic, retain) NSDate *visitDate;
@property (nullable, nonatomic, retain) NSString *visitId;

@end

NS_ASSUME_NONNULL_END
