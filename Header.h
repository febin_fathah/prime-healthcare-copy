//
//  Header.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef Header_h
#define Header_h


#import "Helpers.h"
#import "ViewControllers.h"
#import "defaults.h"
#import "Categories.h"
#import "Config.h"
#import "PUtilities.h"
#import "DBModels.h"


#endif /* Header_h */
