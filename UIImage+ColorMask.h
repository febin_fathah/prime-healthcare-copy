//
//  UIImage+ColorMask.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 26/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorMask)

+ (UIImage *)ipMaskedImageNamed:(NSString *)name color:(UIColor *)color;
+(UIImage *)ipMaskedWithImage:(UIImage *)image color:(UIColor *)color;
@end
