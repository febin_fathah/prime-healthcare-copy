//
//  HospitalImageCell.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HospitalImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *hospitalImageView;

@end
