//
//  DoctorTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *qualification;
@property (weak, nonatomic) IBOutlet UILabel *department;

@end
