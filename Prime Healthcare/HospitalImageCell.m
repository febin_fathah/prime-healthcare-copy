//
//  HospitalImageCell.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "HospitalImageCell.h"

@implementation HospitalImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _hospitalImageView.layer.cornerRadius = 5.0f;
    _hospitalImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _hospitalImageView.layer.borderWidth = 1.2f;
    _hospitalImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
