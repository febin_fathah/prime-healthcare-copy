//
//  DoctorDetailsTableViewCell.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "DoctorDetailsTableViewCell.h"
#import "Header.h"

@implementation DoctorDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_AppointButton setImage:[UIImage ipMaskedWithImage:_AppointButton.imageView.image color:[UIColor whiteColor]] forState:UIControlStateNormal] ;
    [_FavButton setImage:[UIImage ipMaskedWithImage:_FavButton.imageView.image color:[UIColor whiteColor]] forState:UIControlStateNormal] ;
    
    [_phoneimage setImage:[UIImage imageNamed:@"dr.call"]];
    [_emailimage setImage:[UIImage imageNamed:@"dr.email"]];
    
    [_phoneimage setBackgroundColor:[UIColor colorWithHexString:DOCTORE_PHONE]];
    [_emailimage setBackgroundColor:[UIColor colorWithHexString:DOCTORE_EMAIL]];
    
    _phoneimage.layer.cornerRadius=13.0f;
    _emailimage.layer.cornerRadius=13.0f;
    _headerView.layer.cornerRadius=5.0f;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
