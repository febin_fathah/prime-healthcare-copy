//
//  LoginViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"
#import "MGTBasicHttpBinding_IPrimeAppService.h"
#import "CCUBasicHttpBinding_IPrimeAppService.h"
#import "CCUServiceResult.h"
#import "CCUArrayOfServiceResult.h"
#import "USLBasicHttpBinding_IPrimeAppService.h"
#import "USLUserProfile.h"
#import "USLArrayOfUserProfile.h"
#import "MGTLoginDetails.h"
#import "MGTArrayOfLoginDetails.h"
#import "Header.h"

@interface LoginViewController ()<CMESoapServiceResponse,MGTSoapServiceResponse,CCUSoapServiceResponse,USLSoapServiceResponse,UITextFieldDelegate>


#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@end

@implementation LoginViewController{
    NSString *deviceID;
}


#pragma mark - ViewController methods
- (void)viewDidLoad {
    [super viewDidLoad];
    //uuuid
    deviceID = [[NSUserDefaults standardUserDefaults]valueForKey:@"DEVICE_ID"];
    // Do any additional setup after loading the view from its nib.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Util
- (void)setup
{
    self.view.layer.cornerRadius = 2.0;
    self.view.clipsToBounds = YES;
    _signinButton.layer.cornerRadius = 3.0;
    _signinButton.clipsToBounds = YES;
    _signupButton.layer.cornerRadius = 3.0;
    _signupButton.clipsToBounds = YES;
    _username.delegate=self;
    _username.tag=333;
}

#pragma mark - Button Actions
- (IBAction)signupClicked:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickSignupButton:)]) {
        
        [self.delegate didClickSignupButton:self];
    }
    
    
    
}
- (IBAction)signinClicked:(id)sender {
    if ([_username.text isEqualToString:@""]) {
        [self showAlert:@"Error!" message:@"Enter registration number"];
        return;
    }
    if ([_password.text isEqualToString:@""]) {
        [self showAlert:@"Error!" message:@"Enter password"];
        return;
    }
    else{
        
        
        if ([self isInternetConnection]) {
            [self.view endEditing:YES];

            [SVProgressHUD showWithStatus:@"authenticating..."];
 
            NSString *uname=[NSString stringWithFormat:@"RG%@",_username.text];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                MGTBasicHttpBinding_IPrimeAppService *service=[MGTBasicHttpBinding_IPrimeAppService new];
                [service GetDeviceInfoAsync:uname Password:_password.text TransType:@"GET_DEVICE_INFO" __target:self];
            });
           
            
        }
        else{
            [self showNoInternetAlert];
        }
    }



}
- (IBAction)forgotClicked:(id)sender {
    
    
    [_delegate didClickForgotPasswordBtn:self];
    
    

        }
- (IBAction)skipClicked:(id)sender {
    [_delegate didClickSkipButton];
}

- (void)dismissView
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark - Forgot password delegate
-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    
       dispatch_async(dispatch_get_main_queue(), ^{
           [SVProgressHUD dismiss];
           if ([methodName isEqualToString:@"GetDeviceInfo"]) {
               NSLog(@"%@",methodName);
               MGTArrayOfLoginDetails *result=(MGTArrayOfLoginDetails *)value;
               if (result.items.count!=0) {
                   MGTLoginDetails *loginUser=(MGTLoginDetails *)[result.items objectAtIndex:0];
                   
                   if([loginUser.result isEqualToString:@"200"]){
                       
                       
                       //device Id will be compared with the current device Id locally, and if same, login and call UpdatePushToken service
                       if ([deviceID isEqualToString:loginUser.DeviceId]) {
                           NSString *rgNo=[NSString stringWithFormat:@"RG%@",_username.text];

                           [self updatePushTokenServiceWithUserId:rgNo];
                        
                       }
                       else{
                           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"warning"message:@"Please confirm to make this is your default device.Any record of previous user will be cleared" preferredStyle:UIAlertControllerStyleAlert];
                           UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                               NSString *rgNo=[NSString stringWithFormat:@"RG%@",_username.text];
                               
                               [self updatePushTokenServiceWithUserId:rgNo];
                               
                               
                           }];
                           UIAlertAction* cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                               
                           }];
                           [alertController addAction:ok];
                           [alertController addAction:cancel];
                           
                           [self presentViewController:alertController animated:YES completion:nil];
                           

                           
                       }
                       
                   }
                   else{
                       
                       [self showAlert:@"Login Failed" message:loginUser.message];
                   }
                   
               }
               else{
                   [self showAlert:@"Alert" message:@"Login failed, please try again"];
                   
               }
               
               
               
           }
           
           
           if ([methodName isEqualToString:@"UpdatePushToken"]) {
               
               NSLog(@"%@",methodName);
               
               CCUArrayOfServiceResult *responce=(CCUArrayOfServiceResult *)value;
               CCUServiceResult *result=(CCUServiceResult *)[responce.items objectAtIndex:0];
               
               if ([result.result intValue] == [SUCCESS_RESPONCE intValue]) {
                   [self getUserProfile];
               }
               else{
                   [self showAlert:@"Failed" message:@"Login failed,please try again"];
               }
           }
           
           if ([methodName isEqualToString:@"GetUserProfile"]) {
               
               USLArrayOfUserProfile *responce=(USLArrayOfUserProfile *)value;
               USLUserProfile *user=(USLUserProfile *)[responce.items objectAtIndex:0];
               if (user) {
                   [SVProgressHUD dismiss];
                   NSString *rgNo=[NSString stringWithFormat:@"RG%@",_username.text];
                   NSDictionary *dic=[[NSDictionary alloc]initWithObjectsAndKeys:user.PatientName,@"name",_password.text,@"password",user.MobileNo,@"mobile",rgNo,@"regNo",  nil];
                   [PUtilities setUserInfo:dic];
                   [UserProfile saveUserDetails:user];
                     [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLogged"];
                   [[NSUserDefaults standardUserDefaults]synchronize];
                   
                   [[NSNotificationCenter defaultCenter]postNotificationName:@"RELOAD_SIDE_MENU" object:nil];
                   [self showAlert:@"Success" message:@"Logged in successfully"];
                   
                   [_delegate loginCompletedSuccessfully];
                  
               }
            
           }
           
           
           

       });
   }
-(void)onError:(NSError *)error{
    NSLog(@"%@",error);

}


-(void)updatePushTokenServiceWithUserId:(NSString *)regID{
    [self.view endEditing:YES];
    [SVProgressHUD showWithStatus:@"registering device..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CCUBasicHttpBinding_IPrimeAppService *pushTokenService=[CCUBasicHttpBinding_IPrimeAppService new];
        
        NSString *fcmToken=[[NSUserDefaults standardUserDefaults]valueForKey:@"FCM_TOKEN"];
        
        [pushTokenService UpdatePushTokenAsync:deviceID FCMToken:fcmToken DeviceType:@"1" RGNo:regID TransType:@"UPDATE_PUSH_TOKEN " __target:self];
        
        
    });
  
    
}

-(void)getUserProfile{
    [self.view endEditing:YES];
    [SVProgressHUD showWithStatus:@"saving user..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        USLBasicHttpBinding_IPrimeAppService *getUserProfile=[USLBasicHttpBinding_IPrimeAppService new];
        NSString *rgNo=[NSString stringWithFormat:@"RG%@",_username.text];

        [getUserProfile GetUserProfileAsync:rgNo TransType:@"GET_USER_PROFILE" __target:self];
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- Text Feild delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag==333) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength < 7;
    }
    else
        return YES;
    
}
@end
