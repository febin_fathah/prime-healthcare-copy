//
//  MyInformationViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/5/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "MyInformationViewController.h"
#import "FutureInfoTableViewCell.h"
#import "PastVisitTableViewCell.h"
#import "FavoriteDoctorTableViewCell.h"
#import "PatientInfoTableViewCell.h"
#import "Header.h"

#define BTN_PATIENTS_APPOINMENT 100
#define BTN_PAST_VISIT 101
#define BTN_FUTURE_APPOINMENT 102
#define BTN_FAV_DOCTOR 103
#define BTN_MED_DETAILS 104
#define BTN_LAB_RESULTS 105
#define BTN_LOYALITY_PATIENTS 106

#define FEEDBACK_BTN_TAG 9999
@interface MyInformationViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UserProfile *userProfile;
}
@property (strong, nonatomic) IBOutlet UITableView *futureTableView;
@property (strong, nonatomic) IBOutlet UIView *headerSection;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;

@end


@implementation MyInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _futureTableView.rowHeight = UITableViewAutomaticDimension;
    _futureTableView.estimatedRowHeight = 200.0f;
    
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    userProfile = [UserProfile getUserProfile];
    
}

-(void)viewWillAppear:(BOOL)animated{
    //
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Tableview Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

//    if (_type==BTN_FUTURE_APPOINMENT) {
//        static NSString *cellIdentifier=@"futureCell";
//        
//        FutureInfoTableViewCell *cell=(FutureInfoTableViewCell *) [_futureTableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        
//        if (cell==nil) {
//            //Set Default Properties
//        }
//        
//        return cell;
//    }
//    
//    else if (_type==BTN_PAST_VISIT){
//        static NSString *cellIdentifier=@"pastVisitCell";
//        
//        PastVisitTableViewCell *cell=(PastVisitTableViewCell *) [_futureTableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        
//        if (cell==nil) {
//            //Set Default Properties
//        }
//        [cell.btnFeedBack setTag:FEEDBACK_BTN_TAG+indexPath.row];
//        [cell.btnFeedBack addTarget:self action:@selector(addFeedBack:) forControlEvents:UIControlEventTouchUpInside];
//        return cell;
//        
//        
//    }
//    else if (_type==BTN_FAV_DOCTOR){
//        static NSString *cellIdentifier=@"favoriteCell";
//        
//        FavoriteDoctorTableViewCell *cell=(FavoriteDoctorTableViewCell *) [_futureTableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        
//        if (cell==nil) {
//            //Set Default Properties
//        }
//        
//        return cell;
//        
//    }
//    
//    else if (_type==BTN_PATIENTS_APPOINMENT){
        static NSString *cellIdentifier=@"pateientInfoCell";
        
        PatientInfoTableViewCell *cell=(PatientInfoTableViewCell *) [_futureTableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell==nil) {
            //Set Default Properties
            cell = [[PatientInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.nameLabel.text = userProfile.patientName;
        cell.idLable.text = userProfile.emiratesID;
        cell.regNumberLable.text = userProfile.regNo;
        cell.mobileNumberLabel.text = userProfile.mobileNo;
        cell.emailLabel.text = userProfile.email;
        cell.dobLabel.text = userProfile.dateOfBirth;
        
        return cell;
        
//    }
//    else{
//        static NSString *cellIdentifier=@"futureCell";
//        
//        FutureInfoTableViewCell *cell=(FutureInfoTableViewCell *) [_futureTableView dequeueReusableCellWithIdentifier:cellIdentifier];
//        
//        if (cell==nil) {
//            //Set Default Properties
//        }
//        
//        return cell;
//    }
    
    
}





#pragma mark - Segue Methdos
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"addFeedback"]) {
        
 
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
