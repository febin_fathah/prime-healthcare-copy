//
//  AboutContentViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@interface AboutContentViewController : BaseViewController
@property NSUInteger pageIndex;
@property (weak, nonatomic) IBOutlet UIPageControl *ControllerIndicator;

@property(nonatomic,weak) NSString *contentValue;
@end
