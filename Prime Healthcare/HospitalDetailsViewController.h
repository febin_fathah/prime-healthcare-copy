//
//  HospitalDetailsViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "BETHospitalDetails.h"


@interface HospitalDetailsViewController : BaseViewController
@property(nonatomic,weak) BETHospitalDetails *hospital;

@end
