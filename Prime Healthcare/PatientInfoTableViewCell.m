//
//  PatientInfoTableViewCell.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/8/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "PatientInfoTableViewCell.h"
#import "Header.h"

@implementation PatientInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _baseView.layer.cornerRadius = 3.0f;
    _baseView.layer.borderWidth = 1.0f;
    _baseView.layer.borderColor = UIColorFromRGB(0xDCDCDC).CGColor;
    _baseView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
