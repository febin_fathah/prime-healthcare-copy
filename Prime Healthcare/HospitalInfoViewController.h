//
//  HospitalInfoViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface HospitalInfoViewController : BaseViewController<MKMapViewDelegate,CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;

}
@property (weak, nonatomic) IBOutlet UITableView *ListTableview;
@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet UIImageView *SearchImage;


@end
