//
//  AboutUsCell.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "AboutUsCell.h"
#import "Header.h"

@implementation AboutUsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    TTTAttributedLabel *lable = [TTTAttributedLabel new];
    _contacts.enabledTextCheckingTypes = NSTextCheckingAllSystemTypes;
    _contacts.linkAttributes = @{ NSForegroundColorAttributeName : UIColorFromRGB(0xE75203),
                                 NSUnderlineStyleAttributeName : @(NSUnderlineStyleNone) };
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
