//
//  SideMenuTableViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 18/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "SideMenuTableViewController.h"
#import "MenuTableViewCell.h"
#import "MFSideMenu.h"
#import "HeaderTableViewCell.h"

@interface SideMenuTableViewController ()
{
    NSArray *TitleArray;
    NSArray *ImageArray;

}

@end

@implementation SideMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TitleArray = [[NSArray alloc]initWithObjects:@"Home",@"My Information",@"Doctors Profile",@"Hospital & Clinic Info",@"Health Tips",@"Vaccination Calculator",@"BMI Calculator",@"Chat",@"Contact",@"About the App", nil];
    
    ImageArray = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"menu-1"],[UIImage imageNamed:@"menu-2"],[UIImage imageNamed:@"menu-3"],[UIImage imageNamed:@"menu-4"],[UIImage imageNamed:@"menu-7"],[UIImage imageNamed:@"menu-9"],[UIImage imageNamed:@"menu-10"],[UIImage imageNamed:@"menu-11"],[UIImage imageNamed:@"menu-12"],[UIImage imageNamed:@"menu-13"], nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadSideMenu) name:@"RELOAD_SIDE_MENU" object:nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"view will appear");
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return TitleArray.count+1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
    if (indexPath.row==0) {
        if (isLoggedIn) {
            return 75;
        }
        else{
            return 50;
        }
    }
    else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.row==0) {
        HeaderTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        cell.logBtn.layer.cornerRadius=12.5;
        cell.logoImg.layer.cornerRadius=10;
        cell.baseView.layer.cornerRadius=10;

        [cell.logBtn addTarget:self action:@selector(logAction:) forControlEvents:UIControlEventTouchUpInside];
        BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
        if (isLoggedIn) {
            NSDictionary *savedData=[[NSUserDefaults standardUserDefaults]objectForKey:@"CURRENT_USER"];
            [cell.logBtn setTitle:@"Logout" forState:UIControlStateNormal];
            [cell.name setHidden:NO];

            cell.name.text=[savedData valueForKey:@"name"];
            cell.verticalConstrain.constant=15;
            cell.btnWidthConstrain.constant=80;
        }
        else{
            [cell.logBtn setTitle:@"Login" forState:UIControlStateNormal];
            [cell.name setHidden:YES];

            cell.verticalConstrain.constant=0;
            cell.btnWidthConstrain.constant=75;


        }
        return cell;
        
    }
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    
    cell.titleOutlet.text = [TitleArray objectAtIndex:indexPath.row-1];
    cell.ImgOutlet.image = [ImageArray objectAtIndex:indexPath.row-1];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *destNav;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
     if ([TitleArray[indexPath.row-1] isEqualToString:@"Hospital & Clinic Info"]){
         destNav = [storyboard instantiateViewControllerWithIdentifier:@"HosptllViewID"];
     }
     else if ([TitleArray[indexPath.row-1] isEqualToString:@"My Information"]){
         BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];

         if (isLoggedIn) {
             destNav = [storyboard instantiateViewControllerWithIdentifier:@"myInfo"];

         }
         else{
             [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"DID_SKIP_PRESSED"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];

         }

     }
     else if ([TitleArray[indexPath.row-1] isEqualToString:@"Doctors Profile"]){
     
      destNav = [storyboard instantiateViewControllerWithIdentifier:@"DoctViewID"];
     }
     else if ([TitleArray[indexPath.row-1] isEqualToString:@"About the App"]){
         
         destNav = [storyboard instantiateViewControllerWithIdentifier:@"AboutID"];
     }
    
     else if ([TitleArray [indexPath.row-1] isEqualToString:@"BMI Calculator"])
     {
         destNav = [storyboard instantiateViewControllerWithIdentifier:@"bmiCalculator"];
     }
    
    else
    {
        destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
    }
    [self.menuContainerViewController setCenterViewController:destNav];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)logAction:(UIButton *)btn{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *destNav;
    if ([btn.titleLabel.text isEqualToString:@"Login"]) {

        destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"DID_SKIP_PRESSED"];
        [[NSUserDefaults standardUserDefaults]synchronize];

        [self.tableView reloadData];

        [self.menuContainerViewController setCenterViewController:destNav];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if([btn.titleLabel.text isEqualToString:@"Logout"]){
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isLogged"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"DID_SKIP_PRESSED"];

        [[NSUserDefaults standardUserDefaults]synchronize];

        destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
        [self.tableView reloadData];

        [self.menuContainerViewController setCenterViewController:destNav];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

    }
    
    
}
#pragma mark- load sideMenu

-(void)reloadSideMenu{
    [self.tableView reloadData];

    
}
@end
