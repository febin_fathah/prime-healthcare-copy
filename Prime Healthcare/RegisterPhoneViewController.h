//
//  RegisterPhoneViewController.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "ASTUserRegistrationRequest.h"
@class RegisterPhoneViewController;
@protocol RegisterPhoneViewControllerDelegate <NSObject>

@required
- (void)didPressRegisterButtonWith:(NSArray *)patientsList from:(RegisterPhoneViewController *)vc;
- (void)didClickSkipButton;
- (void)didPressRegisterButtonWithSinglePatient:(ASTUserRegistrationRequest *)patient from:(RegisterPhoneViewController *)vc;


@end

@interface RegisterPhoneViewController : BaseViewController

@property (strong, nonatomic) id <RegisterPhoneViewControllerDelegate>delegate;

@end
