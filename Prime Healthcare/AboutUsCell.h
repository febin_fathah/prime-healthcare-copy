//
//  AboutUsCell.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBackgroundView.h"
#import "TTTAttributedLabel.h"

@interface AboutUsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLocation;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *contacts;

@property (weak, nonatomic) IBOutlet UILabel *titleContact;
@property (weak, nonatomic) IBOutlet UILabel *titleRegTiming;
@property (weak, nonatomic) IBOutlet UILabel *rgTimings;
@property (weak, nonatomic) IBOutlet UILabel *websiteURL;


@property (weak, nonatomic) IBOutlet CellBackgroundView *backGround;
@end
