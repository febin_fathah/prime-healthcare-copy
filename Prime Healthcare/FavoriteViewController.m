//
//  FavoriteViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FavoriteViewController.h"
#import "Header.h"
#import "FavoriteDoctorTableViewCell.h"
#import "FQPFavouriteDoctor.h"


@interface FavoriteViewController ()<UITableViewDelegate, UITableViewDataSource, FQPSoapServiceResponse>{
    NSMutableArray *dataArray;
    BOOL isLoaded;

}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 200.0f;
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    self.title = @"My Information";
//    [self getAllAppontments];
    [self getFavoriteDoctors];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)getFavoriteDoctors{
    dataArray = [[FavoriteDoctor getAllFavoriteDoctors]  mutableCopy];
    if (dataArray.count>0) {
        [_tableView reloadData];
    }
    else{
        [self getDataFromServer];
    }
    
}

- (void)getDataFromServer{
    [SVProgressHUD showWithStatus:@"Loading..."];
    FQPBasicHttpBinding_IPrimeAppService* service = [[FQPBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging=TRUE;
    [service GetFavouriteDoctorAsync:[PUtilities getRegId] TransType:@"" __target:self];
}

#pragma mark -TableView Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([dataArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        });
        
        return 1;
        
    } else {
        // Display a message when the table is empty
        if (isLoaded) {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
            
            messageLabel.text = @"No record is  available.";
            messageLabel.textColor = UIColorFromRGB(0xE85206);
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [messageLabel sizeToFit];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.backgroundView = messageLabel;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            });
        }
    }
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"favoriteCell";
    
    FavoriteDoctorTableViewCell *cell=(FavoriteDoctorTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        //Set Default Properties
        cell = [[FavoriteDoctorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    FavoriteDoctor *doctor = [dataArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = doctor.doctorName;
    cell.specialiyLabel.text = doctor.qualification;
    cell.cardiologyLabel.text = doctor.departmentName;
    NSLog(@"%@",doctor.doctorPhoto);
    [cell.profilePic sd_setImageWithURL:[NSURL URLWithString:doctor.doctorPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
    
    [cell.favButton addTarget:self action:@selector(favButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

- (void)favButtonClicked:(UIButton *)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    FavoriteDoctor *favDoctor = [dataArray objectAtIndex: indexPath.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Remove from your favorites?"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Loading..."];
        FQPBasicHttpBinding_IPrimeAppService *service = [[FQPBasicHttpBinding_IPrimeAppService alloc] init];
        [service AddRemoveFavouriteDoctorAsyncWithBlock:[PUtilities getRegId] DoctorId:favDoctor.doctorId DoctorName:favDoctor.doctorName TransType:@"R_MAPPING_MAPPING" __handler:^(id obj) {
            if ([obj isKindOfClass:[FQPArrayOfServiceResult class]]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                   [dataArray removeObject:favDoctor];
                    [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                    [FavoriteDoctor addFavoriteDoctor:favDoctor.doctorId add:NO];
                    [_tableView reloadData];
                });
            }
        }];
         
    }];
    [alertController addAction:ok];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}



- (void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    if ([methodName isEqualToString:@"GetFavouriteDoctor"]) {
        FQPArrayOfFavouriteDoctor * res = (FQPArrayOfFavouriteDoctor *)value;
        if (res.count > 0) {
            for (FQPFavouriteDoctor *doctor in res) {
                [FavoriteDoctor addFavoriteDoctor:doctor.DoctorId add:YES];
                
            }
            dataArray = [[FavoriteDoctor getAllFavoriteDoctors]  mutableCopy];
            [_tableView reloadData];
        }
        isLoaded = YES;
        [_tableView reloadData];
        [SVProgressHUD dismiss];
    }
}

- (void)onError:(NSError *)error{
    isLoaded = YES;
    [_tableView reloadData];
    [self showAlert:@"Error!" message:error.localizedDescription];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
