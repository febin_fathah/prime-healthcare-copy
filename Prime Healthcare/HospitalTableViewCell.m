//
//  HospitalTableViewCell.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "HospitalTableViewCell.h"

@implementation HospitalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _ImageOutlet.layer.cornerRadius=_ImageOutlet.frame.size.height/2;
    _ImageOutlet.clipsToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
