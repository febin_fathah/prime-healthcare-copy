//
//  ViewControllers.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef ViewControllers_h
#define ViewControllers_h

#import "LoginViewController.h"
#import "RegisterPhoneViewController.h"
#import "LoggedUserViewController.h"

#import "AboutAppViewController.h"
#import "AboutContentViewController.h"


#import "HospitalInfoViewController.h"
#import "HospitalDetailsViewController.h"
#import "MakaniViewController.h"

#import "DoctorReadTableViewCell.h"
#import "DoctorImageTableViewCell.h"
#import "DoctorDetailsTableViewCell.h"
#import "DoctorProfileViewController.h"

#import "PastVisitViewController.h"
#import "MedicationViewController.h"
#import "FeedbackViewController.h"



#endif /* ViewControllers_h */
