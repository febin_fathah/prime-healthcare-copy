//
//  RegisterPhoneViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "RegisterPhoneViewController.h"
#import "ASTBasicHttpBinding_IPrimeAppService.h"
#import "ASTArrayOfUserRegistrationRequest.h"
#import "ASTUserRegistrationRequest.h"
#import "SelectProfileViewController.h"
#import "Header.h"
#import "ForgotPasswordViewController.h"
#define K_REGISTERATION @"1"
#define K_FORGOT_PASSWORD @"0"
@interface RegisterPhoneViewController ()<ASTSoapServiceResponse,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITextField *mobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UITextField *countryCodes;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeSelected;
@end

@implementation RegisterPhoneViewController{
    NSArray *patientsList;
    SelectProfileViewController *selectProfile;
    UITextField *selectCountry;
    UILabel *selectedCode;
    ForgotPasswordViewController *SignupViewController;
    NSArray *codes ;
}

#pragma mark - ViewController Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    _mobileNumber.text=@"";
    // Do any additional setup after loading the view from its nib.
    [self setUp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Util
- (void)setUp
{
    self.view.layer.cornerRadius = 2.0;
    self.view.clipsToBounds = YES;
    _registerButton.layer.cornerRadius = 3.0;
    _registerButton.clipsToBounds = YES;
    _mobileNumber.delegate=self;
    
    codes = [[NSArray alloc]initWithObjects:@"050",@"052",@"055",@"056", nil];
    [_countryCodes setDelegate:self];
    
    UIPickerView *codePicker = [[UIPickerView alloc] init];
    [codePicker setBackgroundColor:[UIColor whiteColor]];
    [codePicker setValue:BASE_COLOR forKey:@"textColor"];
    [codePicker setDataSource: self];
    [codePicker setDelegate: self];
    codePicker.showsSelectionIndicator = YES;
    codePicker.tag=3001;
    _countryCodes.inputView=codePicker;
}

#pragma mark - Actions
- (IBAction)registerButtonClicked:(id)sender {
    
    [self.view endEditing:YES];
    if ([_countryCodeSelected.text isEqualToString:@""]) {
        [self showAlert:@"Alert" message:@"Please choose your country code"];

    }
    
    else if ([_mobileNumber.text isEqualToString:@""]) {
        
        [self showAlert:@"Error!" message:@"Enter a valid mobile number"];
    }
    else{
        
        if ([self isInternetConnection]) {

            [SVProgressHUD showWithStatus:@"Registering..."];
            ASTBasicHttpBinding_IPrimeAppService *service=[ASTBasicHttpBinding_IPrimeAppService new];
            //0559863226
            NSString *number=[NSString stringWithFormat:@"%@%@",_countryCodeSelected.text,_mobileNumber.text];
            [service GetPatientListForRegistrationAsync:number TransType:@"GET_PATIENT_LIST" __target:self];
        }
        else{
            [self showNoInternetAlert];
        }
        
    }
    
    
    
    

    
}
- (IBAction)skipButtonClicked:(id)sender {
    [_delegate didClickSkipButton];
}

#pragma mark - SOAP Responce
-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    NSLog(@"%@",methodName);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        if ([methodName isEqualToString:@"GetPatientListForRegistration"]) {
            
            ASTArrayOfUserRegistrationRequest *result=(ASTArrayOfUserRegistrationRequest *)value;
            if (result.items.count!=0) {
                
                if (result.items.count==1) {
                    ASTUserRegistrationRequest *patient=[ASTUserRegistrationRequest new];
                    patient=[result.items objectAtIndex:0];
                    [_delegate didPressRegisterButtonWithSinglePatient:patient from:self] ;
                    
                }
                else{
                [_delegate didPressRegisterButtonWith:result.items from:self];
                }
            }
            else{
                
                [self showAlert:@"Registration failed!" message:@"No user available for register"];
            }
        }
    });
   

}
-(void)onError:(NSError *)error{
    NSLog(@"%@",error);
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag==888) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 7;
    }
    else
        return YES;

}

#pragma mark - Picker view delegate


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
        return codes.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  
        return [codes objectAtIndex:row];
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
        _countryCodeSelected.text=[codes objectAtIndex:row];
        [_countryCodes resignFirstResponder];
    
 
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

