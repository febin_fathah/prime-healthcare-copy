//
//  HeaderCell.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBackgroundView.h"


@interface HeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CellBackgroundView *backGround;
@end
