//
//  HospitalDetailsViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "HospitalDetailsViewController.h"
// cells
#import "ServiceListTableViewCell.h"
#import "HospitalImageCell.h"
#import "AboutUsCell.h"
#import "ButtonCell.h"
#import "HeaderCell.h"


#import "Header.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface HospitalDetailsViewController ()<UITableViewDelegate,UITableViewDataSource, MFMailComposeViewControllerDelegate,TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITableView *detailsTableView;

@end

@implementation HospitalDetailsViewController{
    NSArray *servicesList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.navigationItem.title=@"Hospital Info";
    // Do any additional setup after loading the view.
    
    self.title=@"Hospital info";
    self.navigationItem.leftBarButtonItem=[self setUpBackButton];
    _nameLabel.text = _hospital.ClinicName;
//    [_hospitalImage sd_setImageWithURL:[NSURL URLWithString:_hospital.HospitalPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
//    NSString *str;
//
//    NSScanner *scanner = [NSScanner scannerWithString:_hospital.ContactInfo];
//    [scanner scanUpToString:@"|" intoString:&str];
//    
//    NSString *phno = [[str componentsSeparatedByCharactersInSet:
//                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                           componentsJoinedByString:@""];
//    _phNo.text= phno;
    servicesList=[NSArray new];
    servicesList=[_hospital.DepartmentName componentsSeparatedByString:@","];
//
//    _aboutHospitalView.layer.cornerRadius = 3.0f;
//    _aboutHospitalView.clipsToBounds = YES;
    _detailsTableView.layer.cornerRadius = 5.0f;
    _detailsTableView.clipsToBounds = YES;
    //_hospitalAboutLabel.text = @"No data available";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableViewMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return servicesList?servicesList.count+1:0;
            break;

        default:
            return 0;
            break;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // image Section
    
    if (indexPath.section == 0) {
        static NSString *cellidentifier=@"ImageCell";
        
        HospitalImageCell *cell = (HospitalImageCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (!cell) {
            cell = [[HospitalImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        }
        
        [cell.hospitalImageView sd_setImageWithURL:[NSURL URLWithString:_hospital.HospitalPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
        
        
        return cell;
    }
    // Button Section
    else if (indexPath.section == 1) {
        static NSString *cellidentifier=@"ButtonCell";
        
        ButtonCell *cell = (ButtonCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (!cell) {
            cell = [[ButtonCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        }
        [cell.locationButton addTarget:self action:@selector(mapNavigation:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.phoneButton addTarget:self action:@selector(callACTION:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.mailButton addTarget:self action:@selector(mailAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.makaniButton addTarget:self action:@selector(makaniAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    // About Section
    else if (indexPath.section == 2) {
        static NSString *cellidentifier=@"AboutCell";
        
        AboutUsCell *cell = (AboutUsCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (!cell) {
            cell = [[AboutUsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        }
        
        cell.backGround.position = CellPositionSingle;
        cell.backGround.borderColor = BORDERCOLOR;
        cell.backGround.fillColor = [UIColor whiteColor];
        
//        cell.address.text = _hospital.
//        if ([_hospital.StreetName isEqualToString:@""]) {
//            cell.titleLocation.text = @"";
//            cell.address.text = @"";
//        }
//        else{
            cell.titleLocation.text = @"Location";
            cell.address.text = _hospital.StreetName;
//        }
        
//        if ([_hospital.ContactInfo isEqualToString:@""]) {
//            cell.titleContact.text = @"";
//            cell.contacts.text = @"";
//        }
//        else{
            cell.titleContact.text = @"Contacts";
//            cell.contacts.text = NO;
            cell.contacts.text = _hospital.ContactInfo;
        cell.contacts.delegate = self;
//        }
        
        if ([_hospital.RegistrationTimings isEqualToString:@""]) {
            cell.titleRegTiming.text = @"";
            cell.rgTimings.text = @"";
        }
        else{
            cell.titleRegTiming.text = @"Registration timing";
//            cell.rgTimings.hidden = NO;
            cell.rgTimings.text = [NSString stringWithFormat:@"Timing on week days: %@\nTiming on fridays: %@\nTiming on saturdays: %@",_hospital.TimingsOnWeekdays,_hospital.TimingsOnFridays, _hospital.TimingsOnSaturdays];;
        }
        cell.websiteURL.text = @"www.primehealth.ae";
        
        return cell;
    }
    
    // service section
    else {
        if (indexPath.row==0) {
            static NSString *cellidentifier=@"headerCell";
            
            HeaderCell *cell = (HeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
            if (!cell) {
                cell = [[HeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
            }
            ;
            cell.backGround.fillColor = UIColorFromRGB(0xED8933);
            cell.backGround.borderColor = BORDERCOLOR;
            cell.backGround.position = CellPositionTop;
            
            return cell;
        }
        else{
            static NSString *cellidentifier=@"serviceCell";
            
            ServiceListTableViewCell *cell = (ServiceListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
            
            
            if (!cell) {
                cell = [[ServiceListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
                
            }
            if (servicesList.count == indexPath.row+1) {
                cell.backGround.position = CellPositionBottom;
                cell.backGround.borderColor = BORDERCOLOR;
                cell.backGround.fillColor = [UIColor whiteColor];
            }
            else{
                cell.backGround.position = CellPositionMiddle;
                cell.backGround.borderColor = BORDERCOLOR;
                cell.backGround.fillColor = [UIColor whiteColor];
            }
            
            cell.serviceList.text=[NSString stringWithFormat:@"%@",[servicesList objectAtIndex:indexPath.row-1]];
            
           
            
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 ) {
        return CGRectGetWidth(tableView.frame)*3/4;
    }
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (NSMutableAttributedString *)contactStringMake{
    NSMutableAttributedString *contactString;
    NSString *email;
    NSArray *contacts = [NSArray new];
    contacts=[_hospital.ContactInfo componentsSeparatedByString:@"|"];
    for (NSString *string in contacts) {
        if ([string containsString:@"Email:"]) {
            email = [string stringByReplacingOccurrencesOfString:@"Email:" withString:@""];
            NSLog(@"email: %@",email);
            [self openMailComposer:email];
        } else {
            NSLog(@"string does not contain bla");
        }
    }
    
    return contactString;
}


- (IBAction)callACTION:(id)sender {
    
    NSString *str;

    NSScanner *scanner = [NSScanner scannerWithString:_hospital.ContactInfo];
        [scanner scanUpToString:@"|" intoString:&str];
    
        NSString *phno = [[str componentsSeparatedByCharactersInSet:
                                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                               componentsJoinedByString:@""];
    NSLog(@"Phone : %@", phno);
    [self initCall:[@"tel://" stringByAppendingString:phno]];
   
}
-(void)initCall:(NSString *)phone{
//    NSString *phoneNumber = [@"tel://" stringByAppendingString:phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"MakaniSegue"]) {
        MakaniViewController *vc = [segue destinationViewController];
        NSArray* words = [_hospital.Makani componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* nospacestring = [words componentsJoinedByString:@""];
        vc.makaniNumber = nospacestring;
    }
}

- (IBAction)mapNavigation:(id)sender {
    //lat-25.2707763-long-55.32221179999999
//    CLLocationCoordinate2D centre;
//    centre.latitude = 25.2707763;    // getting latitude
//    centre.longitude = 55.32221179999999;  // getting longitude
//    
//    CLLocationCoordinate2D sample;
//    sample.latitude = 60.2707763;    // getting latitude
//    sample.longitude = 60.32221179999999;
//    
//  //dummy data
//    NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",sample.longitude,sample.latitude,centre.longitude,centre.latitude];
//    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([_hospital.LatitudeLocation  doubleValue],[_hospital.LongtitudeLocation doubleValue]);
    
        //Apple Maps, using the MKMapItem class
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
        item.name = _hospital.ClinicName;
        [item openInMapsWithLaunchOptions:nil];
    
}
- (IBAction)mailAction:(id)sender {
//    ContactInfo	__NSCFString *	@"Tel. No:04-2720720  |  Fax No.:04-2720702 | Email:helpdesk@primehealth.ae"
    NSString *email;
    NSArray *contacts = [NSArray new];
    contacts=[_hospital.ContactInfo componentsSeparatedByString:@"|"];
    for (NSString *string in contacts) {
        if ([string containsString:@"Email:"]) {
            email = [string stringByReplacingOccurrencesOfString:@"Email:" withString:@""];
            NSLog(@"email: %@",email);
            [self initCall:[NSString stringWithFormat:@"mailto://%@",email]];
        } else {
            NSLog(@"string does not contain bla");
        }
    }
}
- (IBAction)makaniAction:(id)sender {
    [self performSegueWithIdentifier:@"MakaniSegue" sender:self];
    
}
#pragma mark - open mail composer
-(void) openMailComposer:(NSString *) email
{
    // Email Subject
    NSString *emailTitle =@"";
    // Email Content
    NSString *messageBody = nil;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:email];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:YES];
    [mc setToRecipients:toRecipents];
    
    if (mc) {
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [SVProgressHUD showSuccessWithStatus:@"Discarded email"];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [SVProgressHUD showSuccessWithStatus:@"Saved email"];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [SVProgressHUD showSuccessWithStatus:@"sent email"];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [SVProgressHUD showErrorWithStatus:@"Failed to send email"];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark TTTattributedLabel delegate
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    if ([self isEmail:[NSString stringWithFormat:@"%@",url]]) {
        [self initCall:[NSString stringWithFormat:@"%@",url]];
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithPhoneNumber:(NSString *)phoneNumber{
    [self initCall:[@"tel://" stringByAppendingString:phoneNumber]];
}

-(BOOL)isEmail:(NSString *)text{
    NSString *regex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [emailPredicate evaluateWithObject:text];
}



@end
