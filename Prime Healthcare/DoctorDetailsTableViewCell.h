//
//  DoctorDetailsTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *FavButton;
@property (weak, nonatomic) IBOutlet UIButton *AppointButton;
@property (weak, nonatomic) IBOutlet UIImageView *phoneimage;
@property (weak, nonatomic) IBOutlet UIImageView *emailimage;
@property (weak, nonatomic) IBOutlet UILabel *phoneNum;
@property (weak, nonatomic) IBOutlet UILabel *mailID;
@property (weak, nonatomic) IBOutlet UILabel *qualification;
@property (weak, nonatomic) IBOutlet UILabel *specialisation;
@property (weak, nonatomic) IBOutlet UILabel *nameDoctor;
@property (weak, nonatomic) IBOutlet UILabel *hospitalName;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@end
