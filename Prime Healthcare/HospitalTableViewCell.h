//
//  HospitalTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HospitalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ImageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *AddressLabel;

@end
