//
//  MenuCollectionViewCell.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "MenuCollectionViewCell.h"

@implementation MenuCollectionViewCell
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
}
@end
