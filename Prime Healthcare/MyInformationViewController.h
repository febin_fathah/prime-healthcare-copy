//
//  MyInformationViewController.h
//  Prime Healthcare
//  Created by macbook pro on 8/5/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@interface MyInformationViewController : BaseViewController

@property(nonatomic)  NSInteger type;
@property(nonatomic,weak) NSString *heading;

@end
