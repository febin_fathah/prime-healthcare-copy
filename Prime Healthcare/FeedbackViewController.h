//
//  FeedbackViewController.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "PastVisit.h"

@interface FeedbackViewController : BaseViewController

@property (nonatomic, strong) PastVisit * visit;


@end
