//
//  ForgotScreen1ViewController.h
//  Prime Healthcare
//
//  Created by macbook pro on 9/2/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
@class ForgotScreen1ViewController;
@protocol ForgotScreen1ViewControllerDelegate<NSObject>

@optional
-(void)recivedOTP:(NSString *)otp andRGNumber:(NSString *)regno fromForgotScreen1ViewController:(ForgotScreen1ViewController *)vc;
@end

@interface ForgotScreen1ViewController : BaseViewController
@property (assign, nonatomic) id <ForgotScreen1ViewControllerDelegate>delegate;

@end
