//
//  ForgotPasswordViewController.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/22/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ASTUserRegistrationRequest.h"


@interface ForgotPasswordViewController : BaseViewController


@property(nonatomic,weak) ASTUserRegistrationRequest *selectedPatient;
@property(nonatomic,weak) NSString *TYPE;
@property(nonatomic,weak) NSString *otpRecieved;
@property(nonatomic, weak) NSString *regNumber;
@property (nonatomic, weak) NSString *mobileNumeber;

@end
