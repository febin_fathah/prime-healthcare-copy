//
//  ViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 18/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//


#import "HomeViewController.h"
#import "MenuCollectionViewCell.h"
#import "Header.h"
#import "SelectProfileViewController.h"
#import "ForgotPasswordViewController.h"
#import "ASTUserRegistrationRequest.h"
#import "ImageSliderView.h"
#import "ImageSliderContent.h"
#import "USLUserProfile.h"
#import "LoggedUserViewController.h"
#import "ForgotScreen1ViewController.h"

#define BTN_FIND_DOCTOR 555
#define BTN_FIND_CLINIC 556
#define BTN_HEALTH_TIPS 557
#define BTN_MY_APPOINMENTS 558
#define BTN_EMERGENCY 559
#define BTN_MY_INFORMATION 560
#define kMJScrollViewTag 999

#define K_REGISTERATION @"1"
#define K_FORGOT_PASSWORD @"0"
//#define SIZE 250.0

#define SIZE_FOR_5S 220.0
#define SIZE_FOR_6S 250.0
typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

@interface HomeViewController ()<LoginViewControllerDelegate, RegisterPhoneViewControllerDelegate,SelectProfileDelegate,ForgotScreen1ViewControllerDelegate,LoggedUserViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>
{
 
    
    
    
    ForgotScreen1ViewController *forgotPwdScreen1;
    NSMutableArray   *_myPageDataArray;
    
    NSInteger pageIndex;

    CGFloat SIZE;

    __weak IBOutlet UIPageControl *pageControl;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollConentSize;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *baseMenuHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderScrollViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderScrollViewWidth;

@property(nonatomic,strong) UIViewController *lastVc;

@property(nonatomic,strong)RegisterPhoneViewController *registerViewController;
@property(nonatomic,strong)SelectProfileViewController *selectProfile;
@property(nonatomic,strong)ForgotPasswordViewController *SignupViewController;
@end


@implementation HomeViewController
@synthesize registerViewController;
@synthesize SignupViewController;
@synthesize selectProfile;
- (void)viewDidLoad {
    [super viewDidLoad];

    
    // Do any additional setup after loading the view, typically from a nib.
    
    [self registerNib];
    [self authenticateUser];
    
    [self setupView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
 
    NSLog(@"view will appear");
    
    
    //this is to manage the go back funtionality on mj pop up by holding the last VC
    if (_lastVc) {
        
        if ([_lastVc isKindOfClass:[RegisterPhoneViewController class]]) {
            [self presentPopupViewController:registerViewController animationType:MJPopupViewAnimationFade];
        }
        
        if ([_lastVc isKindOfClass:[LoggedUserViewController class]]) {
            [self presentPopupViewController:selectProfile animationType:MJPopupViewAnimationFade];
        }
        
        if ([_lastVc isKindOfClass:[ForgotScreen1ViewController class]]) {
              [self presentPopupViewController:forgotPwdScreen1 animationType:MJPopupViewAnimationFade];
        }
    }

 
}

-(void)viewDidLayoutSubviews{
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self setUpScrollView];
    
}
-(void)setupView{
    //    calculating scrolview item size
    carouselView.scrollView = _scrollView;
    _scrollConentSize.constant=self.view.frame.size.height;
    
    SIZE = CGRectGetWidth(self.view.frame);
    SIZE = SIZE - SIZE *.25;
    _sliderScrollViewWidth.constant=SIZE;
    _sliderScrollViewHeight.constant=SIZE;
    
    
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
}
- (void)setUpScrollView{
    
    for (UIView *view in _scrollView.subviews) {
        [view removeFromSuperview];
    }
    
//    [_scrollView setFrame:CGRectMake(0,0, SIZE, SIZE)];
    NSLog(@"Frame: %@",NSStringFromCGPoint(carouselView.center));
    _scrollView.center = carouselView.center;
    
    CGPoint center = _scrollView.center;
    center.y = center.y - 54;
    _scrollView.center = center;
    
   NSLog(@"Frame: %@",NSStringFromCGPoint(_scrollView.center));
    
    _arrCarouselItems= [NSArray arrayWithObjects:[ImageSliderContent initWithBundlewithImage:@"hospital-1"],[ImageSliderContent initWithBundlewithImage:@"hospital-2"],[ImageSliderContent initWithBundlewithImage:@"hospital-3"],[ImageSliderContent initWithBundlewithImage:@"hospital-4"], nil];
    pageControl.numberOfPages=_arrCarouselItems.count;
    for (int index = 0; index < [_arrCarouselItems count]; index++)
    {
        ImageSliderContent *item = [_arrCarouselItems objectAtIndex:index];
        
        item.tag=999;
        CGFloat x =     (SIZE * index);
        CGFloat side =  (index == 0) ? SIZE : (SIZE * 0.75);
        
        item.frame =    CGRectMake(x, 0, side, side);
        item.center =   CGPointMake((x + (SIZE/2)), (SIZE/2));
        
        [_scrollView addSubview:item];
    }
    
    
//    [_scrollView setFrame:CGRectMake(0,0, SIZE, SIZE)];
    [_scrollView setContentSize:CGSizeMake(SIZE * [_arrCarouselItems count], CGRectGetHeight(_scrollView.frame))];
//    [_scrollView setCenter:carouselView.center];
    [_scrollView setDelegate:self];
    [self setCurrentPage:1 animater:NO];
}


- (void)setCurrentPage:(NSUInteger)page animater:(BOOL)animated{
    pageControl.currentPage = page;
    [_scrollView scrollRectToVisible:CGRectMake((SIZE * page)- (page+1 * 0.75), 0, SIZE, SIZE) animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LeftMenuAction:(id)sender {
 
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)authenticateUser{
    //[self loadSideMenu];
    BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
    if (!isLoggedIn) {
        
        NSDictionary *savedData=[[NSUserDefaults standardUserDefaults]objectForKey:@"CURRENT_USER"];
      
        if (savedData!=nil) {
            if (![[NSUserDefaults standardUserDefaults]boolForKey:@"DID_SKIP_PRESSED"]) {

            LoggedUserViewController *vc=[[LoggedUserViewController alloc]initWithNibName:@"LoggedUserViewController" bundle:nil];
            vc.currentUser=savedData;
                vc.delegate=self;
            [self presentPopupViewController:vc animationType:MJPopupViewAnimationFade];
            }
        }
        else{
            if (![[NSUserDefaults standardUserDefaults]boolForKey:@"DID_SKIP_PRESSED"]) {
                LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                viewController.delegate = self;
                [self presentPopupViewController:viewController animationType:MJPopupViewAnimationFade];
            }
        }
        
        
        
    }
    else{
      
    }
    
}


#pragma mark - Register Nibs
-(void)registerNib{
    //Register xib files
    
    registerViewController = [[RegisterPhoneViewController alloc] initWithNibName:@"RegisterPhoneViewController" bundle:nil];
    registerViewController.delegate = self;
    
    
       
    selectProfile=[[SelectProfileViewController alloc]initWithNibName:@"SelectProfileViewController" bundle:nil];
    selectProfile.delegate=self;
    
    
    //forgotpassword and signup has same design
    SignupViewController = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    
    forgotPwdScreen1=[[ForgotScreen1ViewController alloc] initWithNibName:@"ForgotScreen1ViewController" bundle:nil];
    forgotPwdScreen1.delegate=self;

    
    
}
-(void)initNibForImageSlider{
     _arrCarouselItems= [NSArray arrayWithObjects:[ImageSliderContent initWithBundlewithImage:@"hospital-1"],[ImageSliderContent initWithBundlewithImage:@"hospital-2"],[ImageSliderContent initWithBundlewithImage:@"hospital-3"],[ImageSliderContent initWithBundlewithImage:@"hospital-4"], nil];
}
/*- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((collectionView.frame.size.width/3), (collectionView.frame.size.height/2));
    
}*/
#pragma mark- SKIP DELEGATE
-(void)didClickSkipButton
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"DID_SKIP_PRESSED"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)loginCompletedSuccessfully
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"DID_SKIP_PRESSED"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}


#pragma mark - LoginView delegate
-(void)didClickSignupButton:(LoginViewController *)viewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    [self presentPopupViewController:registerViewController animationType:MJPopupViewAnimationFade];
}
-(void)didClickForgotPasswordBtn:(LoginViewController *)vc{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    [self presentPopupViewController:forgotPwdScreen1 animationType:MJPopupViewAnimationFade];
}


#pragma mark - RegisterViewdelegate methods


-(void)didPressRegisterButtonWith:(NSArray *)patientsList from:(RegisterPhoneViewController *)vc
{
   // [SVProgressHUD showWithStatus:@"Fetching..."];
    /**/
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    selectProfile.patientsList=patientsList;
    _lastVc = vc;
    [self.navigationController pushViewController:selectProfile animated:YES];
//    [self presentPopupViewController:selectProfile animationType:MJPopupViewAnimationFade];

    
}
-(void)didPressRegisterButtonWithSinglePatient:(ASTUserRegistrationRequest *)patient from:(RegisterPhoneViewController *)vc{
  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    SignupViewController.selectedPatient=patient;
    SignupViewController.TYPE=K_REGISTERATION;
    _lastVc=vc;
    [self.navigationController pushViewController:SignupViewController animated:YES];
//    [self presentPopupViewController:SignupViewController animationType:MJPopupViewAnimationFade];
}
#pragma mark - SelectProfile Delegate methods
-(void)selectedProfileWithPatient:(ASTUserRegistrationRequest *)patient from:(SelectProfileViewController *)vc{
  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    SignupViewController.selectedPatient=patient;
    SignupViewController.TYPE=K_REGISTERATION;
    _lastVc=vc;
    [self.navigationController pushViewController:SignupViewController animated:YES];
    //    [self presentPopupViewController:SignupViewController animationType:MJPopupViewAnimationFade];
}
#pragma mark- Already Logged user
-(void)didClickForgotButton:(LoggedUserViewController *)vc{
    
    
}

-(void)recivedOTP:(NSString *)otp withRegisterNumber:(NSString *)regNo fromLoggedUserViewController:(LoggedUserViewController *)vc{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    SignupViewController.otpRecieved=otp;
    SignupViewController.TYPE=K_FORGOT_PASSWORD;
    SignupViewController.regNumber = regNo;
    _lastVc=vc;
    //[self presentViewController:SignupViewController animated:YES completion:nil];
    
    [self.navigationController pushViewController:SignupViewController animated:YES];
}
#pragma mark- Forgot password
-(void)recivedOTP:(NSString *)otp andRGNumber:(NSString *)regno fromForgotScreen1ViewController:(ForgotScreen1ViewController *)vc{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    SignupViewController.otpRecieved=otp;
    SignupViewController.TYPE=K_FORGOT_PASSWORD;
    SignupViewController.regNumber = regno;
    _lastVc= vc;
    //[self presentViewController:SignupViewController animated:YES completion:nil];
    
    [self.navigationController pushViewController:SignupViewController animated:YES];
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    
    return cell;
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView.tag == 10001) {
       
        for (int index = 0; index < [_arrCarouselItems count]; index++)
        {
            ImageSliderContent *slider = [_arrCarouselItems objectAtIndex:index];
            if (scrollView.contentOffset.x > (SIZE * (index + 1)) ||
                scrollView.contentOffset.x < (SIZE * (index - 1)))
            {// if offset is before / after the page bounds of current button, skip and move to the next page
                
              //  NSLog(@"index-------%d",index);
                continue;
            }
            
            // adjust size of buttons to the LEFT of UIScrollView
            
            if (scrollView.contentOffset.x > (index * SIZE))
            {
                CGRect frame = slider.frame;
                
                frame.size.width = SIZE - ((scrollView.contentOffset.x - (index * SIZE)) * 0.25);
                frame.size.height = SIZE - ((scrollView.contentOffset.x - (index * SIZE)) * 0.25);
                
                slider.frame = frame;
                slider.center = CGPointMake(((SIZE * index) + (SIZE/2)), (SIZE/2));
               NSLog(@" item-------%d",index);

                continue;
            }
            
            // adjust size of buttons to the RIGHT of UIScrollView
            
            if (scrollView.contentOffset.x < (index * SIZE))
            {
                CGRect frame = slider.frame;
                
                frame.size.width = SIZE + ((scrollView.contentOffset.x - (index * SIZE)) * 0.25);
                frame.size.height = SIZE + ((scrollView.contentOffset.x - (index * SIZE)) * 0.25);
                
                slider.frame = frame;
                slider.center = CGPointMake(((SIZE * index) + (SIZE/2)), (SIZE/2));
                //NSLog(@" item-------%d",index);

            }
        }
        
        
        
      
    }
    
    
    // [self setTitle];
}


- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.tag == 10001) {

    long currentPage = lround((float)scrollView.contentOffset.x / scrollView.frame.size.width);
        NSLog(@"%ld",currentPage);

    pageControl.currentPage = currentPage;
        
    }
}

#pragma mark - Home menu Action
- (IBAction)homeMenu:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *destNav;
    if (btn.tag==BTN_FIND_DOCTOR) {
        destNav = [storyboard instantiateViewControllerWithIdentifier:@"DoctViewID"];

    }
    if (btn.tag==BTN_FIND_CLINIC) {
        destNav = [storyboard instantiateViewControllerWithIdentifier:@"HosptllViewID"];

    }
    if (btn.tag==BTN_HEALTH_TIPS) {
        
    }
    if (btn.tag==BTN_MY_APPOINMENTS) {
        
        BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
        
        if (isLoggedIn) {
//            AppointmentsView
            destNav = [storyboard instantiateViewControllerWithIdentifier:@"AppointmentsView"];
        }
        else{
            //DID_SKIP_PRESSED is used to manage the popup
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"DID_SKIP_PRESSED"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
            
        }

        
    }
    if (btn.tag==BTN_EMERGENCY) {
        
    }
    if (btn.tag==BTN_MY_INFORMATION) {
        
        BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
        
        if (isLoggedIn) {
            destNav = [storyboard instantiateViewControllerWithIdentifier:@"myInfo"];
            
        }
        else{
            //DID_SKIP_PRESSED is used to manage the popup 
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"DID_SKIP_PRESSED"];
            [[NSUserDefaults standardUserDefaults]synchronize];
          
            destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
            
        }
        

        

    }
    
    [self.menuContainerViewController setCenterViewController:destNav];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
    
}
@end
