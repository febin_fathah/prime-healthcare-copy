//
//  FutureInfoTableViewCell.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/5/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBackgroundView.h"


@interface FutureInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet CellBackgroundView *baseView;
@property (weak, nonatomic) IBOutlet UILabel *doctorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *qualificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *specializationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *branchLabel;
@property (weak, nonatomic) IBOutlet UIImageView *doctorProfilePhoto;

@end
