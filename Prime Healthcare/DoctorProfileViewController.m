//
//  DoctorProfileViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 26/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "DoctorProfileViewController.h"
#import "Header.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface DoctorProfileViewController ()<UITableViewDelegate,UITableViewDataSource, MFMailComposeViewControllerDelegate, FQPSoapServiceResponse, LoggedUserViewControllerDelegate, LoginViewControllerDelegate>
{
    BOOL ExpandCell;
}
@end

@implementation DoctorProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Doctor Profile";
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];

    // Do any additional setup after loading the view.
   [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
   [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  maark - Tableview delegates
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            DoctorImageTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"imgid"];
            [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:_doctor.doctorPhoto] placeholderImage:[UIImage imageNamed:@"holder_doc"]];
            return cell;
        }
            break;
        case 1:
        {
            DoctorDetailsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"detailsid"];
            
            if (!cell) {
                cell = [[DoctorDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"detailsid"];
            }
            
            cell.nameDoctor.text=_doctor.doctorName;
            cell.qualification.text=_doctor.qualification;
            cell.specialisation.text=_doctor.departmentName;
            cell.phoneNum.text=_doctor.phone;
            cell.mailID.text=_doctor.email;
            cell.hospitalName.text = _doctor.clinicName;
            if ([_doctor.favoriteAdded boolValue]) {
                [cell.FavButton setTitle:@"Remove Favorite" forState:UIControlStateNormal];
                [cell.FavButton setImage:[UIImage imageNamed:@"ic_toggle_star"] forState:UIControlStateNormal];
            }
            else{
                [cell.FavButton setTitle:@"Add to Favorite" forState:UIControlStateNormal];
                [cell.FavButton setImage:[UIImage imageNamed:@"ic_fav"] forState:UIControlStateNormal];
            }
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"]) {
                [cell.FavButton setTitle:@"Add to Favorite" forState:UIControlStateNormal];
                [cell.FavButton setImage:[UIImage imageNamed:@"ic_fav"] forState:UIControlStateNormal];
            }
            [cell.FavButton addTarget:self action:@selector(favAction:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
            break;
            
        case 2:
        {
            DoctorReadTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"readid"];
            cell.readLabel.text =_doctor.discriptionText;
            if (ExpandCell)
                cell.readLabel.numberOfLines=0;
            else
                cell.readLabel.numberOfLines=4;
            
            return cell;
        }
            break;
        
        default:
            return nil;
            break;
    }

}

- (void)favAction:(id)sender{
    
//    Add to favorite
//    call webservice
    BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
    if (!isLoggedIn) {
        [self authenticateUser];
        return;
    }
    
    FQPBasicHttpBinding_IPrimeAppService *service = [[FQPBasicHttpBinding_IPrimeAppService alloc] init];
    if ([_doctor.favoriteAdded boolValue]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Remove from your favorites?"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
             [SVProgressHUD showWithStatus:@"Loading..."];
            [service AddRemoveFavouriteDoctorAsync:[PUtilities getRegId] DoctorId:_doctor.doctorId DoctorName:_doctor.doctorName TransType:@"R_MAPPING_MAPPING" __target:self];
            [FavoriteDoctor addFavoriteDoctor:_doctor.doctorId add:NO];
        }];
        [alertController addAction:ok];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:[NSString stringWithFormat:@"Add to your favorites?"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD showWithStatus:@"Loading..."];
            [service AddRemoveFavouriteDoctorAsync:[PUtilities getRegId] DoctorId:_doctor.doctorId DoctorName:_doctor.doctorName TransType:@"ADD_DOCTOR_MAPPING" __target:self];
            [FavoriteDoctor addFavoriteDoctor:_doctor.doctorId add:YES];
        }];
        [alertController addAction:ok];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    
    
}

-(void)authenticateUser{
    //[self loadSideMenu];
    
    NSDictionary *savedData=[[NSUserDefaults standardUserDefaults]objectForKey:@"CURRENT_USER"];
    if (savedData!=nil) {
        
        LoggedUserViewController *vc=[[LoggedUserViewController alloc]initWithNibName:@"LoggedUserViewController" bundle:nil];
        vc.currentUser=savedData;
        vc.delegate=self;
        [self presentPopupViewController:vc animationType:MJPopupViewAnimationFade];
    }
    else{
        LoginViewController *viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        viewController.delegate = self;
        [self presentPopupViewController:viewController animationType:MJPopupViewAnimationFade];
    }
}


- (void)onError:(NSError *)error{
    [SVProgressHUD dismiss];
    [self showAlert:@"Error!" message:error.localizedDescription];
}

- (void)onSuccess:(id)value methodName:(NSString *)methodName{
    if ([value isKindOfClass:[FQPArrayOfServiceResult class]]) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_tableview reloadData];
            [SVProgressHUD dismiss];
        });
        
        
        
    }
}


#pragma mark - logged viewController delegate methods
- (void)recivedOTP:(NSString *)otp withRegisterNumber:(NSString *)regNo fromLoggedUserViewController:(LoggedUserViewController *)vc{
    
}

#pragma mark - loginViewcontroller delegate
- (void)didClickSkipButton{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"DID_SKIP_PRESSED"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)didClickSignupButton:(LoginViewController *)viewController{
    
}

-(void)didClickForgotPasswordBtn:(LoginViewController *)vc{
    
}

- (void)loginCompletedSuccessfully{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"DID_SKIP_PRESSED"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self favAction:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)readmoreAction:(id)sender {
    ExpandCell = !ExpandCell;
    [_tableview reloadData];
}

- (IBAction)leftmenuAction:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)callAction:(id)sender {
    NSString *phoneNumber = [@"tel://" stringByAppendingString:_doctor.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (IBAction)mailAction:(id)sender {
    
    [self openMailComposer];
}

#pragma mark - open mail composer
-(void) openMailComposer
{
    // Email Subject
    NSString *emailTitle =@"";
    // Email Content
    NSString *messageBody = nil;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:_doctor.email];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:YES];
    [mc setToRecipients:toRecipents];
    
    if (mc) {
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [SVProgressHUD showSuccessWithStatus:@"Discarded email"];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [SVProgressHUD showSuccessWithStatus:@"Saved email"];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [SVProgressHUD showSuccessWithStatus:@"sent email"];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [SVProgressHUD showErrorWithStatus:@"Failed to send email"];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
