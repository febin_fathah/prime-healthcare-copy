//
//  ProfileTableViewCell.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/29/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _profileImage.layer.cornerRadius=_profileImage.frame.size.width/2;
    _profileImage.clipsToBounds=YES;
    
    //addinng orange layer
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_profileImage.frame.size.width), (_profileImage.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:_profileImage.layer.cornerRadius];
    [borderLayer setBorderWidth:1];
    [borderLayer setBorderColor:[[UIColor orangeColor] CGColor]];
    [_profileImage.layer addSublayer:borderLayer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
