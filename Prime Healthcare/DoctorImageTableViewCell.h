//
//  DoctorImageTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoctorImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@end
