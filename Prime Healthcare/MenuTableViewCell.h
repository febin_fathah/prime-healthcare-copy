//
//  MenuTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 20/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ImgOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;

@end
