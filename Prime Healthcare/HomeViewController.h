//
//  ViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 18/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ImageSliderView.h"

@interface HomeViewController : BaseViewController<UIScrollViewDelegate>
{
    NSArray                 *_arrCarouselItems;
    
    __weak IBOutlet ImageSliderView *carouselView;
    IBOutlet UIScrollView   *_scrollView;
    IBOutlet UIView         *_vCarousel;
}
@property (weak, nonatomic) IBOutlet UICollectionView *MenuCollecion;
- (IBAction)LeftMenuAction:(id)sender;


@end

