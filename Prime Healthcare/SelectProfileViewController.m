//
//  SelectProfileViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/23/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "SelectProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "ForgotPasswordViewController.h"
#define K_REGISTERATION @"1"

@interface SelectProfileViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *profilesTableView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
NSString *cellidentifier =@"profileCell";
@implementation SelectProfileViewController{
    ForgotPasswordViewController *SignupViewController;

}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    NSLog(@"%@",_patientsList);
    [SVProgressHUD show];
    self.view.layer.cornerRadius = 2.0;
    self.view.clipsToBounds = YES;
    _profilesTableView.delegate=self;
    _profilesTableView.dataSource=self;
    //forgotpassword and signup has same design
    SignupViewController = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil];
    [_profilesTableView registerNib:[UINib nibWithNibName:@"ProfileTableViewCell" bundle:nil] forCellReuseIdentifier:cellidentifier];
    self.automaticallyAdjustsScrollViewInsets = YES;
//    self.navigationController.navigationBarHidden = NO;
   self.navigationItem.leftBarButtonItem =  [self setUpBackButton];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"SELECT PROFILE";
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table View Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _patientsList.count;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProfileTableViewCell *cell=(ProfileTableViewCell *)[_profilesTableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    ASTUserRegistrationRequest *patient=[ASTUserRegistrationRequest new];
    patient=[_patientsList objectAtIndex:indexPath.row];
    cell.nameLabel.text=patient.PatientName;
    //cell.profileImage.image=[UIImage imageNamed:patient.PatientPhotoUrl];
    return cell;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [SVProgressHUD show];
    ASTUserRegistrationRequest *patient=[ASTUserRegistrationRequest new];
    patient=[_patientsList objectAtIndex:indexPath.row];
    
//   [_delegate selectedProfileWithPatient:patient from:self];
    
//    perform push action to next viewConteoller
    [self performPush:patient];
    
    }
- (void)performPush:(ASTUserRegistrationRequest *) patient{
    
    SignupViewController.selectedPatient=patient;
    SignupViewController.TYPE=K_REGISTERATION;
    self.title = nil;
    [self.navigationController pushViewController:SignupViewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
