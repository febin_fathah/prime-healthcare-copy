//
//  MedicationViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "MedicationViewController.h"
#import "Header.h"
#import "PastVisitTableViewCell.h"
#import "MedicationTableViewCell.h"
#import "RWBPatientMedicationDetails.h"




@interface MedicationViewController (){
    
    BOOL isLoaded;
    RWBArrayOfPatientMedicationDetails *dataArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation MedicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 200.0f;
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    self.title = @"Past Visit";
    [self getMedicationDetailsFetchFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -TableView Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([dataArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        });
        
        return 1;
        
    } else {
        // Display a message when the table is empty
        if (isLoaded) {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
            
            messageLabel.text = @"No Medicine record is  available.";
            messageLabel.textColor = UIColorFromRGB(0xE85206);
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [messageLabel sizeToFit];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.backgroundView = messageLabel;
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            });
           
        }
    }
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count+1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        static NSString *cellIdentifier=@"pastVisitCell";
        
        PastVisitTableViewCell *cell=(PastVisitTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell==nil) {
            //Set Default Properties
            cell = [[PastVisitTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.timeLabel.text = [_visit.visitDate timeStringfromDateTime];
        cell.dateLabel.text = [_visit.visitDate dateStringfromDateTime];
        cell.doctorNameLabel.text = _visit.doctorName;
        cell.qualificationLabel.text = _visit.qualification;
        cell.specializationLabel.text = _visit.speciality;
        cell.branchLabel.text = _visit.branchName;
        NSLog(@"URL: %@",_visit.doctorPhoto);
        [cell.profilePicture sd_setImageWithURL:[NSURL URLWithString:_visit.doctorPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
        return cell;
    }
    else{
        static NSString *cellIdentifier=@"MedicationCell";
        MedicationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        RWBPatientMedicationDetails *medicatiom = [dataArray objectAtIndex:indexPath.row -1];
        cell.medicinename.text =  medicatiom.ItemName;
        cell.detailsLabel.text = medicatiom.Instruction;
        
        return cell;
    }
    
    
}




- (void)getMedicationDetailsFetchFromServer{
    
    [SVProgressHUD showWithStatus:@"Loading..."];
    ;
    NSNumber* param0 = [NSNumber numberWithInteger: [_visit.branchId integerValue]];;//initialization code
    NSNumber* param1 = [NSNumber numberWithInteger: [_visit.visitId integerValue]];;//initialization code
    RWBBasicHttpBinding_IPrimeAppService* service = [[RWBBasicHttpBinding_IPrimeAppService alloc] init];
    NSError *error= nil;
    [service GetMedicationInformationAsyncWithBlock:param0 VisitId:param1  __handler:^(id obj) {
        if([obj isKindOfClass:[RWBArrayOfPatientMedicationDetails class]])
        {
            RWBArrayOfPatientMedicationDetails* res=(RWBArrayOfPatientMedicationDetails*)obj;
            //in res variable you have a value returned from your web service
            dataArray = res;
            [_tableView reloadData];
        }
        else {
            
            [self showAlert:@"Error!" message:error.localizedDescription];
        }
        [SVProgressHUD dismiss];
        isLoaded = YES;
        [_tableView reloadData];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
