//
//  FavoriteDoctorTableViewCell.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/8/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBackgroundView.h"
@interface FavoriteDoctorTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet CellBackgroundView *baseView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *specialiyLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardiologyLabel;
@property (weak, nonatomic) IBOutlet UIButton *appoinmentButton;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIButton *favButton;

@end
