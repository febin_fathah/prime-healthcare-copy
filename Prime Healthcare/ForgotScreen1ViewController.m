//
//  ForgotScreen1ViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 9/2/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "ForgotScreen1ViewController.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"
#import "CMEArrayOfServiceResult.h"
#import "CMEServiceResult.h"
#define SUCCESS_RESPONCE @"200"

@interface ForgotScreen1ViewController ()<CMESoapServiceResponse>
@property (weak, nonatomic) IBOutlet UIButton *submitBTN;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumber;

@end

@implementation ForgotScreen1ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView{
    self.view.layer.cornerRadius = 2.0;
    self.view.clipsToBounds = YES;
    _submitBTN.layer.cornerRadius = 3.0;
    _submitBTN.clipsToBounds = YES;
    
}
- (IBAction)submit:(id)sender {
    [self.view endEditing:YES];
    if ([_mobileNumber.text isEqualToString:@""]) {
        [self showAlert:@"Error!" message:@"Enter valid registration number"];
    }
    else{
        if ([self isInternetConnection]) {
            [SVProgressHUD showWithStatus:@"sending OTP..."];
            CMEBasicHttpBinding_IPrimeAppService *service=[CMEBasicHttpBinding_IPrimeAppService new];
            [service ForgotPasswordAsync:[NSString stringWithFormat:@"RG%@",_mobileNumber.text] OTPPassword:@"" TransType:@"FORGOT_PASSWORD" __target:self];
        }
        else{
            [self showNoInternetAlert];
        }
    
    }
}

#pragma mark- SOAP responce
-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        NSLog(@"%@",value);
    if ([methodName isEqualToString:@"ForgotPassword"]) {
        CMEArrayOfServiceResult *result=(CMEArrayOfServiceResult *)value;
        CMEServiceResult *respoce=(CMEServiceResult *)[result.items objectAtIndex:0];
        if ([respoce.result isEqual:SUCCESS_RESPONCE]) {
            [_delegate recivedOTP:respoce.otpPassword andRGNumber:[NSString stringWithFormat:@"RG%@",_mobileNumber.text] fromForgotScreen1ViewController:self];
        }
        else{
            [self showAlert:@"Alert" message:@"Sending Failed,please try again"];
        }
    }
        
    });
}
-(void)onError:(NSError *)error{
    
}

#pragma mark- Text Feild delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag==222) {
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength < 7;
    }
    else
        return YES;
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
