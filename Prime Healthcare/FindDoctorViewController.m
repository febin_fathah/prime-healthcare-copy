//
//  FindDoctorViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FindDoctorViewController.h"
#import "DoctorTableViewCell.h"
#import "Header.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"
#import "CMEArrayOfDoctorDetails.h"
#import "CMEDoctorDetails.h"
#import "DoctorProfileViewController.h"

#define DOCTOR_FEILD 655
#define ALL_CLINIC 888
#define ALL_DEPARTMENT 999
@interface FindDoctorViewController () <UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate,CMESoapServiceResponse,UITableViewDelegate>
{
    NSArray *allClinic;
    NSArray *Departments;
    NSMutableArray *doctorsList;
    NSArray *sortedArray;
    BOOL isSortArray;

}
@property (weak, nonatomic) IBOutlet UITableView *doctorsTableView;

@end

@implementation FindDoctorViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    [self getAllDoctors];
    [self setUpView];
    self.navigationItem.title=@"Find Doctor";
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
}
-(void)setUpView{
    allClinic = [[NSArray alloc]initWithObjects:@"DE-DEIRA PRIME",@"SZ-ZAYED PRIME",@"AD-ABUDHABI PRIME",@"SM-PRIME ", nil];
    Departments = [[NSArray alloc]initWithObjects:@"AMBULANCE SERVICE",@"ANESTHESHIOLOGY",@"COMSETOTLOGY",@"CARDIOLOGY",@"DENTAL",@"E.N.T",@"GENERAL", nil];
    [_doctorext setTag:DOCTOR_FEILD];
    [_doctorext setDelegate:self];
    
    UIPickerView *departmentPicker = [[UIPickerView alloc] init];
    [departmentPicker setBackgroundColor:[UIColor whiteColor]];
    [departmentPicker setDataSource: self];
    [departmentPicker setDelegate: self];
    departmentPicker.showsSelectionIndicator = YES;
    departmentPicker.tag=ALL_CLINIC;
    _allClinicText.inputView=departmentPicker;
    
    [_allClinicText setDelegate:self];
    UIPickerView *branchPicker = [[UIPickerView alloc] init];
    [branchPicker setBackgroundColor:[UIColor whiteColor]];

    [branchPicker setDataSource: self];
    [branchPicker setDelegate: self];
    branchPicker.showsSelectionIndicator = YES;
    branchPicker.tag=ALL_DEPARTMENT;
    _allDepartmentText.inputView=branchPicker;
    [_allDepartmentText setDelegate:self];
    
    _doctorext.rightViewMode = UITextFieldViewModeAlways;
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search"]];
    [_doctorext setRightView:searchImage];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(serach:)];
    [_doctorext.rightView addGestureRecognizer:tapRecognizer];
    
    _allClinicText.rightViewMode = UITextFieldViewModeAlways;
    _allClinicText.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowbottom"]];
    
    _allDepartmentText.rightViewMode = UITextFieldViewModeAlways;
    _allDepartmentText.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowbottom"]];
}

- (void)search:(id)sender{
    [self filterContentForSearchText:_doctorext.text scope:@""];
}




#pragma mark- text feild delegate


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
//    if (textField.tag==DOCTOR_FEILD) {
//        isSortArray=NO;
//        [self.view endEditing:YES];
//        
//        [_doctorsTableView reloadData];
//    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField.text isEqualToString:@""]) {
        
        isSortArray=NO;
        [self.view endEditing:YES];

        [_doctorsTableView reloadData];
        return YES;
    }
    else{
    [self filterContentForSearchText:textField.text scope:@""];
        [self.view endEditing:YES];

    return YES;
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    
    
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.DoctorName contains[cd] %@",searchText];
    NSArray *filteredArray = [doctorsList filteredArrayUsingPredicate:bPredicate];
    
    sortedArray=[NSArray arrayWithArray:filteredArray];
    isSortArray=YES;
    [self.view endEditing:YES];
    [_doctorsTableView reloadData];
    
    NSLog(@"%@",filteredArray);
    
}


#pragma mark - Tableview delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return doctorsList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [SVProgressHUD dismiss];

    DoctorTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellid"];
    cell.imageOutlet.layer.cornerRadius=25.0f;
    
    FavoriteDoctor *doctor;
    
    doctor=[doctorsList objectAtIndex:indexPath.row];
    
    cell.name.text=doctor.doctorName;
    cell.qualification.text=doctor.qualification;
    cell.department.text=doctor.departmentName;
    [cell.imageOutlet sd_setImageWithURL:[NSURL URLWithString:doctor.doctorPhoto] placeholderImage:[UIImage imageNamed:@"holder_doc"]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"infoSegue" sender:self];
}
-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"infoSegue"]) {
        NSIndexPath *index=[_doctorsTableView indexPathForSelectedRow];
        DoctorProfileViewController *vc=segue.destinationViewController;
        FavoriteDoctor *doctorSelected=[doctorsList objectAtIndex:index.row];
        vc.doctor= doctorSelected;
    }
}
#pragma mark - Picker view delegate


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==ALL_CLINIC)
       return allClinic.count;
    else
       return Departments.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==ALL_CLINIC)
        return [allClinic objectAtIndex:row];
     else
         return [Departments objectAtIndex:row];
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (thePickerView.tag==ALL_CLINIC) {
        _allClinicText.text=[allClinic objectAtIndex:row];
        [_allClinicText resignFirstResponder];
    }
    else
    {
        _allDepartmentText.text=[Departments objectAtIndex:row];
        [_allDepartmentText resignFirstResponder];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LeftbuttonAction:(id)sender {

    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}


#pragma mark - Get Doctor List
- (void)getAllDoctors{
    
    doctorsList = [NSMutableArray arrayWithArray:[FavoriteDoctor getAllDoctors]];
    if ([doctorsList count]> 0) {
        [self.doctorsTableView reloadData];
    }
    else{
        [self getDoctorsList];
    }
}


-(void)getDoctorsList{
    
    
    [SVProgressHUD showWithStatus:@"Fetching..."];
    CMEBasicHttpBinding_IPrimeAppService* service = [CMEBasicHttpBinding_IPrimeAppService new];
    
    [service GetAllDoctorDetailsAsync:self];
    
}

-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    NSLog(@"%@",value);
    NSLog(@"%@",methodName);
    dispatch_async(dispatch_get_main_queue(), ^{

    
    if([value isKindOfClass:[CMEArrayOfDoctorDetails class]])
    {
        CMEArrayOfDoctorDetails* res=(CMEArrayOfDoctorDetails*)value;
        
        if (res.count > 0 ) {
            for (CMEDoctorDetails *doctor in res) {
                [FavoriteDoctor saveDoctor:doctor];
            }
            doctorsList = [NSMutableArray arrayWithArray:[FavoriteDoctor getAllDoctors]];
            [_doctorsTableView reloadData];
        }
        

        [_doctorsTableView reloadData];
        //in res variable you have a value returned from your web service
    }
    else
    {
        //here you can put the code you want to run when your web service operation is finished
    }
    });
}

-(void)onError:(NSError *)error{
    NSLog(@"%@",error);
    [SVProgressHUD dismiss];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
