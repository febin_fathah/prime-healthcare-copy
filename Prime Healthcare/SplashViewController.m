//
//  SplashViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 9/15/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "SplashViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "Header.h"
@interface SplashViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *welcomeText;
@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

   
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];
    
        if (isLoggedIn) {
            NSLog(@"%@",[PUtilities getName]);
            [self addSubviewWithBounce:_nameLabel withData:[PUtilities getName]];
            [self addSubviewWithBounce:_welcomeText withData:@"Welcomes You"];
        
    
    [NSTimer scheduledTimerWithTimeInterval:.9
                                     target:self
                                   selector:@selector(directToHome)
                                   userInfo:nil
                                    repeats:NO];
        }
        else{
            _nameLabel.text=@"";
        }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)directToHome{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    MFSideMenuContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"MenuContainerViewController"];
    AppDelegate *appdelegate=[[UIApplication sharedApplication]delegate];
    appdelegate.window.rootViewController = container;
    NSLog(@"hhh %@",[appdelegate.window.rootViewController class]);
    UINavigationController *destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
    UITableViewController *sideMenu = [storyboard instantiateViewControllerWithIdentifier:@"sidemenuController"];
    [container setCenterViewController:destNav];
    [container setLeftMenuViewController:sideMenu];
    [container setRightMenuViewController:nil];
    
    
}
-(void)addSubviewWithBounce:(UILabel *)theView withData:(NSString *)data
{
    theView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    theView.text=data;
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        theView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
