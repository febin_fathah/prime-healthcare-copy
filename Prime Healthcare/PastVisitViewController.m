//
//  PastVisitViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 28/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "PastVisitViewController.h"
#import "Header.h"
#import "BDHArrayOfPatientVisitDetails.h"
#import "PastVisitTableViewCell.h"
#import "UMGPatientFeedbackDetails.h"

@interface PastVisitViewController ()<UITableViewDelegate, UITableViewDataSource, BDHSoapServiceResponse>{
    
    NSArray *dataArray;
    BOOL isLoaded;
    PastVisit *selectedVisit;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PastVisitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 200.0f;
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    self.title = @"My Information";
    [self getPastVisitDetails];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -TableView Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([dataArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        });
        
        return 1;
        
    } else {
         // Display a message when the table is empty
        if (isLoaded) {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
            
            messageLabel.text = @"No record is  available.";
            messageLabel.textColor = UIColorFromRGB(0xE85206);
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [messageLabel sizeToFit];
            self.tableView.backgroundView = messageLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
    }
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"pastVisitCell";
    
    PastVisitTableViewCell *cell=(PastVisitTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        //Set Default Properties
        cell = [[PastVisitTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    PastVisit *visit = [dataArray objectAtIndex:indexPath.row];
    cell.timeLabel.text = [visit.visitDate timeStringfromDateTime];
    cell.dateLabel.text = [visit.visitDate dateStringfromDateTime];
    cell.doctorNameLabel.text = visit.doctorName;
    cell.qualificationLabel.text = visit.qualification;
    cell.specializationLabel.text = visit.speciality;
    cell.branchLabel.text = visit.branchName;
    NSLog(@"profile image: %@",visit.doctorPhoto);
    [cell.profilePicture sd_setImageWithURL:[NSURL URLWithString:visit.doctorPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
    [cell.feedbackButton addTarget:self action:@selector(addFeedBack:) forControlEvents:UIControlEventTouchUpInside];
    [cell.medicationButton addTarget:self action:@selector(viewMedication:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}


- (void)getPastVisitDetails{
    
//    Fetch from Local if there is no data in local call from web service
    dataArray = [PastVisit getAllVisitDetails];
    isLoaded = YES;
    if (dataArray.count > 0) {
        [_tableView reloadData];
    }
    else{
        [self pastVisitFetchFromServer];
    }
}

- (void)pastVisitFetchFromServer{
    [SVProgressHUD showWithStatus:@"Loading..."];
    BDHBasicHttpBinding_IPrimeAppService* service = [[BDHBasicHttpBinding_IPrimeAppService alloc]init];
    [service GetPastVisitDetailsAsync:[PUtilities getRegId] __target:self];
    
}

- (void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    
    if([value isKindOfClass:[BDHArrayOfPatientVisitDetails class]])
    {
        BDHArrayOfPatientVisitDetails* res=(BDHArrayOfPatientVisitDetails*)value;
        //in res variable you have a value returned from your web service
        if (res.count>0) {
            [PastVisit deleteAllVisitList];
            for (BDHPatientVisitDetails * pastVisit in res) {
                [PastVisit savePastVisit:pastVisit];
            }
            dataArray = [PastVisit getAllVisitDetails];
            [_tableView reloadData];
        }
    }
    else
    {
        //here you can put the code you want to run when your web service operation is finished
        [self showAlert:@"Alert!" message:@"No records available!"];
    }
    [_tableView reloadData];
    [SVProgressHUD dismiss];
}

- (void)onError:(NSError *)error{
    
    [self showAlert:@"Error!" message:error.localizedDescription];
    [SVProgressHUD dismiss];
}


#pragma mark - Cell button methods
- (void)addFeedBack:(id)sender{
    NSLog(@"%s",__func__);
    // Check feedback service available for visit
//    [self performSegueWithIdentifier:@"FeedbackSegue" sender:self];
    [SVProgressHUD showWithStatus:@"Loading..."];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    selectedVisit = [dataArray objectAtIndex:indexPath.row];
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    [service CheckVisitAvailableForFeedbackAsyncWithBlock:[PUtilities getRegId] BranchId:[NSNumber numberWithInt:[selectedVisit.branchId intValue]] VisitId:[NSNumber numberWithInt:[selectedVisit.visitId intValue]] __handler:^(id obj) {
        
        if ([obj isKindOfClass:[UMGArrayOfPatientFeedbackDetails class]]) {
            
            UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
            UMGPatientFeedbackDetails *feedBackDetail = [res.items firstObject];
            if ([feedBackDetail.Result intValue] == 200) {
                // perform segue to feedback page
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [self performSegueWithIdentifier:@"FeedbackSegue" sender:self];
                });
                
            }
            else{
                [SVProgressHUD dismiss];
                [self showAlert:@"Sorry!" message:feedBackDetail.Message];
            }
        }
        else{
            NSError *error = obj;
            [SVProgressHUD dismiss];
            [self showAlert:@"Error!" message:error.localizedDescription];
        }
        
        
        
    }];
    
}

- (void)viewMedication:(id)sender{
    NSLog(@"%s",__func__);
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    selectedVisit = [dataArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"MedicationSegue" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"MedicationSegue"]) {
        MedicationViewController *vc = [segue destinationViewController];
        vc.visit = selectedVisit;
    }
    
    if ([segue.identifier isEqualToString:@"FeedbackSegue"]) {
        FeedbackViewController *vc = [segue destinationViewController];
        vc.visit = selectedVisit;
    }
}


@end
