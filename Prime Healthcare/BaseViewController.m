//
//  BaseViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "Header.h"

@interface BaseViewController ()
@property (nonatomic) Reachability *hostReachability;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithHexString:NAV_TINT_COLOR_HEX]};
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LeftbuttonAction:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark- back button
-(UIBarButtonItem *)setUpBackButton{
    
    [self.navigationController.navigationItem setHidesBackButton:YES];
    
    UIBarButtonItem *backButton =[[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(back:)];
    return  backButton;

}
-(void)back:(UIBarButtonItem *)btn{
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - showAlert

-(void)showAlert:(NSString *)title message:(NSString *)value{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:value preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}
-(void)showNoInternetAlert{
    [self showAlert:@"No internet Connection!" message:@"Please check internet connectivity"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark- Internet Settings

-(BOOL)isInternetConnection{
    Reachability *reach =[Reachability reachabilityWithHostName:@"google.com"];
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable) {
        return NO;
    }
    
    return YES;
    
}
#pragma mark- load sideMenu
-(void)loadSideMenu{
    
    MFSideMenuContainerViewController *sideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuContainerViewController"];
    self.menuContainerViewController.leftMenuViewController = sideMenu;
    
}
@end
