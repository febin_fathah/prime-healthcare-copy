//
//  MakaniViewController.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 24/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@interface MakaniViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSString *makaniNumber;

@end
