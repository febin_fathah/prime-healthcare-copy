//
//  FutureInfoTableViewCell.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/5/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FutureInfoTableViewCell.h"
#define BORDERCOLOR  [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1]
@implementation FutureInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _baseView.position=CellPositionSingle;
    _baseView.borderColor=BORDERCOLOR;
    _baseView.fillColor=[UIColor whiteColor];
    [_baseView.layer setCornerRadius:5];
    self.contentView.backgroundColor = [UIColor clearColor];
    _doctorProfilePhoto.layer.cornerRadius = 25;//CGRectGetHeight(_doctorProfilePhoto.frame)/2;
    _doctorProfilePhoto.clipsToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
