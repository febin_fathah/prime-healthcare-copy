
//
//  FavoriteDoctorTableViewCell.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/8/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FavoriteDoctorTableViewCell.h"
#define BORDERCOLOR  [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1]
@implementation FavoriteDoctorTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _baseView.position=CellPositionSingle;
    _baseView.borderColor=BORDERCOLOR;
    _baseView.fillColor=[UIColor whiteColor];
    [_baseView.layer setCornerRadius:5];
    
    _appoinmentButton.layer.cornerRadius = 3.0f;
    _appoinmentButton.clipsToBounds = YES;
    _profilePic.layer.cornerRadius = 25;
    _profilePic.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
