//
//  ServiceListTableViewCell.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/26/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBackgroundView.h"

@interface ServiceListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *serviceList;
@property (weak, nonatomic) IBOutlet CellBackgroundView *backGround;


@end
