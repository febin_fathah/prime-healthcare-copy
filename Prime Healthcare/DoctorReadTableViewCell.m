//
//  DoctorReadTableViewCell.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 28/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "DoctorReadTableViewCell.h"

@implementation DoctorReadTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _bgView.layer.cornerRadius=5.0f;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)ReadmoreAction:(id)sender {
    _readLabel.numberOfLines=0;
}
@end
