//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 01/09/2016
//
//---------------------------------------------------


#import "USLBasicHttpBinding_IPrimeAppService.h"
#import "USLRequestResultHandler.h"
#import "USLHelper.h"

@implementation USLBasicHttpBinding_IPrimeAppService
@synthesize Url,ShouldAddAdornments,Headers;
@synthesize EnableLogging;

- (id) init {
    if(self = [super init])
    {
        self.Url=@"http://phcg-ws.cloudapp.net:9099/PrimeHealth.svc/PrimeAppServices";
        self.ShouldAddAdornments=YES;
    }
    return self;
}

- (id) initWithUrl: (NSString*) url {
    if(self = [self init])
    {
        self.Url=url;
    }
    return self;
}

-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(USLRequestResultHandler*) requestMgr
{
    [requestMgr sendImplementation:request];
}

-(USLRequestResultHandler*) CreateRequestResultHandler
{
    USLRequestResultHandler* handler= [[USLRequestResultHandler alloc]init:SOAPVERSION_11];
    handler.EnableLogging = EnableLogging;
    return handler;
}

-(NSMutableURLRequest*) createRequest:(NSString *)__soapAction __request:(USLRequestResultHandler*) __request
{
    NSURL *__url = [NSURL URLWithString:Url];
    NSMutableURLRequest *__requestObj = [NSMutableURLRequest requestWithURL:__url];
    [__request prepareRequest:__requestObj];

    [__requestObj addValue: [NSString stringWithFormat:@"\"%@\"", __soapAction] forHTTPHeaderField:@"SOAPAction"];

    for (NSString* key in self.Headers) {
        [__requestObj addValue: [self.Headers objectForKey:key] forHTTPHeaderField:key];
    }
    [__requestObj setHTTPMethod:@"POST"];


    return __requestObj;
}


-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(USLRequestResultHandler*) requestMgr callback:(void (^)(USLRequestResultHandler *)) callback
{
    [requestMgr sendImplementation:request callbackDelegate:callback];
}

-(void) addAdornments:(DDXMLElement*)__methodElement
{
    if(ShouldAddAdornments)
    {
        [__methodElement addAttribute:[DDXMLNode attributeWithName:@"id" stringValue:@"o0"]];
        [__methodElement addAttribute:[DDXMLNode attributeWithName:@"c:root" stringValue:@"1"]];
    }
}

-(NSMutableURLRequest*) createGetUserProfileRequest:(NSString*) RGNo TransType:(NSString*) TransType __request:(USLRequestResultHandler*) __request
{
    DDXMLElement *__methodElement=[__request writeElement:@"GetUserProfile" URI:@"http://tempuri.org/" parent:__request.Body];
    [self addAdornments:__methodElement];
            
    DDXMLElement* __RGNoItemElement=[__request writeElement:RGNo type:[NSString class] name:@"RGNo" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__RGNoItemElement!=nil)
    {
        [__RGNoItemElement setStringValue:RGNo];
    }
            
    DDXMLElement* __TransTypeItemElement=[__request writeElement:TransType type:[NSString class] name:@"TransType" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__TransTypeItemElement!=nil)
    {
        [__TransTypeItemElement setStringValue:TransType];
    }
    
    NSMutableURLRequest* __requestObj= [self createRequest:@"http://tempuri.org/IPrimeAppService/GetUserProfile"  __request:__request];
    return __requestObj;
}
    
-(USLRequestResultHandler*) GetUserProfileAsync:(NSString*) RGNo TransType:(NSString*) TransType __target:(id) __target __handler:(SEL) __handler
{
    USLRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetUserProfileRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(USLRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[USLHelper getResultElement:__request.OutputBody name:@"GetUserProfileResult"];
        __res= (USLArrayOfUserProfile*)[__request createObject:__result type:[USLArrayOfUserProfile class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    IMP imp = [__target methodForSelector:__handler];
    void (*func)(id, SEL,id) = (void *)imp;
    func(__target, __handler,__res);
    }];
    return __request;
}

-(USLRequestResultHandler*) GetUserProfileAsync:(NSString*) RGNo TransType:(NSString*) TransType __target:(id<USLSoapServiceResponse>) __target
{
    USLRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetUserProfileRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(USLRequestResultHandler *__requestMgr) {
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[USLHelper getResultElement:__request.OutputBody name:@"GetUserProfileResult"];
        [__target onSuccess:(USLArrayOfUserProfile*)[__request createObject:__result type:[USLArrayOfUserProfile class]] methodName:@"GetUserProfile"];
    }
    else
    {
        [__target onError:__requestMgr.OutputFault];
    }
    }];
    return __request;
}
-(USLRequestResultHandler*) GetUserProfileAsyncWithBlock:(NSString*) RGNo TransType:(NSString*) TransType __handler:(void(^) (id)) __handler
{
    USLRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetUserProfileRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(USLRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[USLHelper getResultElement:__request.OutputBody name:@"GetUserProfileResult"];
        __res= (USLArrayOfUserProfile*)[__request createObject:__result type:[USLArrayOfUserProfile class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    __handler(__res);
    }];
    return __request;
    }

-(USLArrayOfUserProfile*) GetUserProfile:(NSString*) RGNo TransType:(NSString*) TransType __error:(NSError**) __error
{
    USLRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetUserProfileRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request ];
    if(__request.OutputFault!=nil)
    {
        if(__error)
        {
            *__error=__request.OutputFault;
        }
        return nil;
    }
    DDXMLElement *__result=[USLHelper getResultElement:__request.OutputBody name:@"GetUserProfileResult"];
    return (USLArrayOfUserProfile*)[__request createObject:__result type:[USLArrayOfUserProfile class]];
}

@end
