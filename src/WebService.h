//
//  WebService.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 27/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef WebService_h
#define WebService_h
// Initial subscription and Doctor listing.
#import "CMEBasicHttpBinding_IPrimeAppService.h"
// User details
#import "USLBasicHttpBinding_IPrimeAppService.h"
// Register mobile
#import "ATKBasicHttpBinding_IPrimeAppService.h"
// update pushToken
#import "CCUBasicHttpBinding_IPrimeAppService.h"
// Login details
#import "MGTBasicHttpBinding_IPrimeAppService.h"
// GetPatient list
#import "ASTBasicHttpBinding_IPrimeAppService.h"
// Hospital
#import "BETBasicHttpBinding_IPrimeAppService.h"
// Past visit
#import "BDHBasicHttpBinding_IPrimeAppService.h"
// Future appointments
#import "EDRBasicHttpBinding_IPrimeAppService.h"
// Medication details
#import "RWBBasicHttpBinding_IPrimeAppService.h"
// favorite doctor
#import "FQPBasicHttpBinding_IPrimeAppService.h"
// Feedback system
#import "UMGBasicHttpBinding_IPrimeAppService.h"

#endif /* WebService_h */
