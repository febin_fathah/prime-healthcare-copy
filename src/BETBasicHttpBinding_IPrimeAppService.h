//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 27/09/2016
//
//---------------------------------------------------



#import <Foundation/Foundation.h>

#import "BETArrayOfHospitalDetails.h"
#import "DDXML.h"

@class BETRequestResultHandler;

@protocol BETSoapServiceResponse < NSObject>
- (void) onSuccess: (id) value methodName:(NSString*)methodName;
- (void) onError: (NSError*) error;
@end


@interface BETBasicHttpBinding_IPrimeAppService : NSObject
    
@property (retain, nonatomic) NSDictionary* Headers;
@property (retain, nonatomic) NSString* Url;
@property (nonatomic) BOOL ShouldAddAdornments;
@property BOOL EnableLogging;

- (id) init;
- (id) initWithUrl: (NSString*) url;

-(NSMutableURLRequest*) createGetAllHospitalDetailsRequest:(BETRequestResultHandler*) __request;
-(BETArrayOfHospitalDetails*) GetAllHospitalDetails:(NSError**) __error __attribute__ ((deprecated("UseGetAllHospitalDetailsAsync method instead")));
-(BETRequestResultHandler*) GetAllHospitalDetailsAsync:(id) __target __handler:(SEL) __handler;
-(BETRequestResultHandler*) GetAllHospitalDetailsAsync:(id<BETSoapServiceResponse>) __target;
-(BETRequestResultHandler*) GetAllHospitalDetailsAsyncWithBlock:(void(^) (id)) __handler;
-(BETRequestResultHandler*) CreateRequestResultHandler;   
-(NSMutableURLRequest*) createRequest :(NSString*) soapAction __request:(BETRequestResultHandler*) __request; 
-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(BETRequestResultHandler*) requestMgr; 
-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(BETRequestResultHandler*) requestMgr callback:(void (^)(BETRequestResultHandler *)) callback;

@end
