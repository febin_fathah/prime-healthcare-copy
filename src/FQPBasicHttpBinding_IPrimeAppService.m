//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 5.0.0.2
//
// Created by Quasar Development at 30/09/2016
//
//---------------------------------------------------


#import "FQPBasicHttpBinding_IPrimeAppService.h"
#import "FQPHelper.h"

@implementation FQPBasicHttpBinding_IPrimeAppService
@synthesize Url,ShouldAddAdornments,Headers;
@synthesize EnableLogging;

- (id) init {
    if(self = [super init])
    {
        self.Url=@"http://phcg-ws.cloudapp.net:9099/PrimeHealth.svc/PrimeAppServices";
        self.ShouldAddAdornments=YES;
    }
    return self;
}

- (id) initWithUrl: (NSString*) url {
    if(self = [self init])
    {
        self.Url=url;
    }
    return self;
}

-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(FQPRequestResultHandler*) requestMgr
{
    [requestMgr sendImplementation:request];
}

-(FQPRequestResultHandler*) CreateRequestResultHandler
{
    FQPRequestResultHandler* handler= [[FQPRequestResultHandler alloc]init:SOAPVERSION_11];
    handler.EnableLogging = EnableLogging;
    return handler;
}

-(NSMutableURLRequest*) createRequest:(NSString *)__soapAction __request:(FQPRequestResultHandler*) __request
{
    NSURL *__url = [NSURL URLWithString:Url];
    NSMutableURLRequest *__requestObj = [NSMutableURLRequest requestWithURL:__url];
    [__request prepareRequest:__requestObj];

    [__requestObj addValue: [NSString stringWithFormat:@"\"%@\"", __soapAction] forHTTPHeaderField:@"SOAPAction"];

    for (NSString* key in self.Headers) {
        [__requestObj addValue: [self.Headers objectForKey:key] forHTTPHeaderField:key];
    }
    [__requestObj setHTTPMethod:@"POST"];


    return __requestObj;
}


-(void) sendImplementation:(NSMutableURLRequest*) request requestMgr:(FQPRequestResultHandler*) requestMgr callback:(void (^)(FQPRequestResultHandler *)) callback
{
    [requestMgr sendImplementation:request callbackDelegate:callback];
}

-(void) addAdornments:(DDXMLElement*)__methodElement
{
    if(ShouldAddAdornments)
    {
        [__methodElement addAttribute:[DDXMLNode attributeWithName:@"id" stringValue:@"o0"]];
        [__methodElement addAttribute:[DDXMLNode attributeWithName:@"c:root" stringValue:@"1"]];
    }
}

-(NSMutableURLRequest*) createAddRemoveFavouriteDoctorRequest:(NSString*) RGNo DoctorId:(NSString*) DoctorId DoctorName:(NSString*) DoctorName TransType:(NSString*) TransType __request:(FQPRequestResultHandler*) __request
{
    DDXMLElement *__methodElement=[__request writeElement:@"AddRemoveFavouriteDoctor" URI:@"http://tempuri.org/" parent:__request.Body];
    [self addAdornments:__methodElement];
            
    DDXMLElement* __RGNoItemElement=[__request writeElement:RGNo type:[NSString class] name:@"RGNo" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__RGNoItemElement!=nil)
    {
        [__RGNoItemElement setStringValue:RGNo];
    }
            
    DDXMLElement* __DoctorIdItemElement=[__request writeElement:DoctorId type:[NSString class] name:@"DoctorId" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__DoctorIdItemElement!=nil)
    {
        [__DoctorIdItemElement setStringValue:DoctorId];
    }
            
    DDXMLElement* __DoctorNameItemElement=[__request writeElement:DoctorName type:[NSString class] name:@"DoctorName" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__DoctorNameItemElement!=nil)
    {
        [__DoctorNameItemElement setStringValue:DoctorName];
    }
            
    DDXMLElement* __TransTypeItemElement=[__request writeElement:TransType type:[NSString class] name:@"TransType" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__TransTypeItemElement!=nil)
    {
        [__TransTypeItemElement setStringValue:TransType];
    }
    
    NSMutableURLRequest* __requestObj= [self createRequest:@"http://tempuri.org/IPrimeAppService/AddRemoveFavouriteDoctor"  __request:__request];
    return __requestObj;
}
    
-(FQPRequestResultHandler*) AddRemoveFavouriteDoctorAsync:(NSString*) RGNo DoctorId:(NSString*) DoctorId DoctorName:(NSString*) DoctorName TransType:(NSString*) TransType __target:(id) __target __handler:(SEL) __handler
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createAddRemoveFavouriteDoctorRequest:RGNo DoctorId: DoctorId DoctorName: DoctorName TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"AddRemoveFavouriteDoctorResult"];
        __res= (FQPArrayOfServiceResult*)[__request createObject:__result type:[FQPArrayOfServiceResult class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    IMP imp = [__target methodForSelector:__handler];
    void (*func)(id, SEL,id) = (void *)imp;
    func(__target, __handler,__res);
    }];
    return __request;
}

-(FQPRequestResultHandler*) AddRemoveFavouriteDoctorAsync:(NSString*) RGNo DoctorId:(NSString*) DoctorId DoctorName:(NSString*) DoctorName TransType:(NSString*) TransType __target:(id<FQPSoapServiceResponse>) __target
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createAddRemoveFavouriteDoctorRequest:RGNo DoctorId: DoctorId DoctorName: DoctorName TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"AddRemoveFavouriteDoctorResult"];
        [__target onSuccess:(FQPArrayOfServiceResult*)[__request createObject:__result type:[FQPArrayOfServiceResult class]] methodName:@"AddRemoveFavouriteDoctor"];
    }
    else
    {
        [__target onError:__requestMgr.OutputFault];
    }
    }];
    return __request;
}
-(FQPRequestResultHandler*) AddRemoveFavouriteDoctorAsyncWithBlock:(NSString*) RGNo DoctorId:(NSString*) DoctorId DoctorName:(NSString*) DoctorName TransType:(NSString*) TransType __handler:(void(^) (id)) __handler
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createAddRemoveFavouriteDoctorRequest:RGNo DoctorId: DoctorId DoctorName: DoctorName TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"AddRemoveFavouriteDoctorResult"];
        __res= (FQPArrayOfServiceResult*)[__request createObject:__result type:[FQPArrayOfServiceResult class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    __handler(__res);
    }];
    return __request;
    }

-(FQPArrayOfServiceResult*) AddRemoveFavouriteDoctor:(NSString*) RGNo DoctorId:(NSString*) DoctorId DoctorName:(NSString*) DoctorName TransType:(NSString*) TransType __error:(NSError**) __error
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createAddRemoveFavouriteDoctorRequest:RGNo DoctorId: DoctorId DoctorName: DoctorName TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request ];
    if(__request.OutputFault!=nil)
    {
        if(__error)
        {
            *__error=__request.OutputFault;
        }
        return nil;
    }
    DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"AddRemoveFavouriteDoctorResult"];
    return (FQPArrayOfServiceResult*)[__request createObject:__result type:[FQPArrayOfServiceResult class]];
}
-(NSMutableURLRequest*) createGetFavouriteDoctorRequest:(NSString*) RGNo TransType:(NSString*) TransType __request:(FQPRequestResultHandler*) __request
{
    DDXMLElement *__methodElement=[__request writeElement:@"GetFavouriteDoctor" URI:@"http://tempuri.org/" parent:__request.Body];
    [self addAdornments:__methodElement];
            
    DDXMLElement* __RGNoItemElement=[__request writeElement:RGNo type:[NSString class] name:@"RGNo" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__RGNoItemElement!=nil)
    {
        [__RGNoItemElement setStringValue:RGNo];
    }
            
    DDXMLElement* __TransTypeItemElement=[__request writeElement:TransType type:[NSString class] name:@"TransType" URI:@"http://tempuri.org/" parent:__methodElement skipNullProperty:YES];
    if(__TransTypeItemElement!=nil)
    {
        [__TransTypeItemElement setStringValue:TransType];
    }
    
    NSMutableURLRequest* __requestObj= [self createRequest:@"http://tempuri.org/IPrimeAppService/GetFavouriteDoctor"  __request:__request];
    return __requestObj;
}
    
-(FQPRequestResultHandler*) GetFavouriteDoctorAsync:(NSString*) RGNo TransType:(NSString*) TransType __target:(id) __target __handler:(SEL) __handler
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetFavouriteDoctorRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"GetFavouriteDoctorResult"];
        __res= (FQPArrayOfFavouriteDoctor*)[__request createObject:__result type:[FQPArrayOfFavouriteDoctor class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    IMP imp = [__target methodForSelector:__handler];
    void (*func)(id, SEL,id) = (void *)imp;
    func(__target, __handler,__res);
    }];
    return __request;
}

-(FQPRequestResultHandler*) GetFavouriteDoctorAsync:(NSString*) RGNo TransType:(NSString*) TransType __target:(id<FQPSoapServiceResponse>) __target
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetFavouriteDoctorRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"GetFavouriteDoctorResult"];
        [__target onSuccess:(FQPArrayOfFavouriteDoctor*)[__request createObject:__result type:[FQPArrayOfFavouriteDoctor class]] methodName:@"GetFavouriteDoctor"];
    }
    else
    {
        [__target onError:__requestMgr.OutputFault];
    }
    }];
    return __request;
}
-(FQPRequestResultHandler*) GetFavouriteDoctorAsyncWithBlock:(NSString*) RGNo TransType:(NSString*) TransType __handler:(void(^) (id)) __handler
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetFavouriteDoctorRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request callback:^(FQPRequestResultHandler *__requestMgr) {
    id __res;
    if(__requestMgr.OutputFault==nil)
    {
        DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"GetFavouriteDoctorResult"];
        __res= (FQPArrayOfFavouriteDoctor*)[__request createObject:__result type:[FQPArrayOfFavouriteDoctor class]];
    }
    else
    {
        __res=__requestMgr.OutputFault;
    }
    
    __handler(__res);
    }];
    return __request;
    }

-(FQPArrayOfFavouriteDoctor*) GetFavouriteDoctor:(NSString*) RGNo TransType:(NSString*) TransType __error:(NSError**) __error
{
    FQPRequestResultHandler* __request = [self CreateRequestResultHandler];
    NSMutableURLRequest *__requestObj=[self createGetFavouriteDoctorRequest:RGNo TransType: TransType __request:__request];
    [self sendImplementation:__requestObj requestMgr:__request ];
    if(__request.OutputFault!=nil)
    {
        if(__error)
        {
            *__error=__request.OutputFault;
        }
        return nil;
    }
    DDXMLElement *__result=[FQPHelper getResultElement:__request.OutputBody name:@"GetFavouriteDoctorResult"];
    return (FQPArrayOfFavouriteDoctor*)[__request createObject:__result type:[FQPArrayOfFavouriteDoctor class]];
}

@end
