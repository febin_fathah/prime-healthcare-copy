//
//  NSDate+TimeDifference.m
//  Magnet
//
//  Created by Mobile Developer on 21/09/15.
//  Copyright (c) 2015 Mobile Developer. All rights reserved.
//

#import "NSDate+TimeDifference.h"

@implementation NSDate (TimeDifference)
#define SECOND  1
#define MINUTE  (SECOND * 60)
#define HOUR    (MINUTE * 60)
#define DAY     (HOUR   * 24)
#define WEEK    (DAY    * 7)
#define MONTH   (DAY    * 31)
#define YEAR    (DAY    * 365.24)

/*
 Mysql Datetime Formatted As Time Ago
 Takes in a mysql datetime string and returns the Time Ago date format
 */
+ (NSString *)mysqlDatetimeFormattedAsTimeAgo:(NSString *)mysqlDatetime
{
    //http://stackoverflow.com/questions/10026714/ios-converting-a-date-received-from-a-mysql-server-into-users-local-time
    //If this is not in UTC, we don't have any knowledge about
    //which tz it is. MUST BE IN UTC.
//    mysqlDatetime = [NSString stringWithFormat:@"%@ UTC",mysqlDatetime];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    
    NSDate *date = [formatter dateFromString:mysqlDatetime];
    
    date = [date toLocalTime];
    
//    NSLog(@"Date %@", date);
    
//    return [date formattedAsTimeAgo];
    return [date formattedForMiryde];
    
}


/*
 Formatted As Time Ago
 Returns the date formatted as Time Ago (in the style of the mobile time ago date formatting for Facebook)
 */
- (NSString *)formattedAsTimeAgo
{
    //Now
    NSDate *now = [NSDate date];
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    
    //Should never hit this but handle the future case
    if(secondsSince < 0)
        return @"In The Future";
    
    
    // < 1 minute = "Just now"
    if(secondsSince < MINUTE)
        return @"Just now";
    
    
    // < 1 hour = "x minutes ago"
    if(secondsSince < HOUR)
        return [self formatMinutesAgo:secondsSince];
    
    
    // Today = "x hours ago"
    if([self isSameDayAs:now])
        return [self formatAsToday:secondsSince];
    
    
    // Yesterday = "Yesterday at 1:28 PM"
    if([self isYesterday:now])
        return [self formatAsYesterday];
    
    
    // < Last 7 days = "Friday at 1:48 AM"
    if([self isLastWeek:secondsSince])
        return [self formatAsLastWeek];
    
    
    // < Last 30 days = "March 30 at 1:14 PM"
    if([self isLastMonth:secondsSince])
        return [self formatAsLastMonth];
    
    // < 1 year = "September 15"
    if([self isLastYear:secondsSince])
        return [self formatAsLastYear];
    
    // Anything else = "September 9, 2011"
    return [self formatAsOther];
    
}

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}



#pragma mark - MiRyde
- (NSString *)formattedForMiryde
{
    //Now
    NSDate *now = [NSDate date];
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    
    // Today
    if([self isSameDayAs:now])
        return [self formatAsToday];
    
    
    // Yesterday
    if([self isYesterday:now])
        return [self formatAsYesterdayMiRyde];
  
//    // < Last 7 days = "Friday"
//    if([self isLastWeek:secondsSince])
//        return [self formatAsWeek];
    
    
    // < Last 30 days = "March 30"
    if([self isLastYear:secondsSince])
        return [self formatAsMonth];
    
    return [self formatAsYear];
}

// Today = "x hours ago"/ "1:28 PM"
- (NSString *)formatAsToday
{
   return @"Today";
}


// Yesterday = "Yesterday"
- (NSString *)formatAsYesterdayMiRyde
{
    return @"Yesterday";
}


// < Last 7 days = "Friday"
- (NSString *)formatAsWeek
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"d EEEE"];
    return [dateFormatter stringFromDate:self];
}


// < Last 30 days = "March 30"
- (NSString *)formatAsMonth
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"MMMM d, yyyy"];
    return [dateFormatter stringFromDate:self];
}


// < 1 year = "September 15, 2015"
- (NSString *)formatAsYear
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}



#pragma mark - Comparison Methods

/*
 ========================== Date Comparison Methods ==========================
 */

/*
 Is Same Day As
 Checks to see if the dates are the same calendar day
 */
- (BOOL)isSameDayAs:(NSDate *)comparisonDate
{
    //Check by matching the date strings
    NSDateFormatter *dateComparisonFormatter = [[NSDateFormatter alloc] init];
    [dateComparisonFormatter setDateFormat:@"yyyy-MM-dd"];
    
    //Return true if they are the same
    return [[dateComparisonFormatter stringFromDate:self] isEqualToString:[dateComparisonFormatter stringFromDate:comparisonDate]];
}




/*
 If the current date is yesterday relative to now
 Pasing in now to be more accurate (time shift during execution) in the calculations
 */
- (BOOL)isYesterday:(NSDate *)now
{
    return [self isSameDayAs:[now dateBySubtractingDays:1]];
}


- (NSDate *) dateBySubtractingDays: (NSInteger) numDays
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + DAY * -numDays;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}


/*
 Is Last Week
 We want to know if the current date object is the first occurance of
 that day of the week (ie like the first friday before today
 - where we would colloquially say "last Friday")
 ( within 6 of the last days)
 
 TODO: make this more precise (1 week ago, if it is 7 days ago check the exact date)
 */
- (BOOL)isLastWeek:(NSTimeInterval)secondsSince
{
    return secondsSince < WEEK;
}


/*
 Is Last Month
 Previous 31 days?
 TODO: Validate on fb
 TODO: Make last day precise
 */
- (BOOL)isLastMonth:(NSTimeInterval)secondsSince
{
    return secondsSince < MONTH;
}


/*
 Is Last Year
 TODO: Make last day precise
 */

- (BOOL)isLastYear:(NSTimeInterval)secondsSince
{
    return secondsSince < YEAR;
}

/*
 =============================================================================
 */





/*
 ========================== Formatting Methods ==========================
 */


// < 1 hour = "x minutes ago"
- (NSString *)formatMinutesAgo:(NSTimeInterval)secondsSince
{
    //Convert to minutes
    int minutesSince = (int)secondsSince / MINUTE;
    
    //Handle Plural
    if(minutesSince == 1)
        return @"1 minute ago";
    else
        return [NSString stringWithFormat:@"%d minutes ago", minutesSince];
}


// Today = "x hours ago"/ "1:28 PM"
- (NSString *)formatAsToday:(NSTimeInterval)secondsSince
{
//    returns "x hours ago"
//    //Convert to hours
    int hoursSince = (int)secondsSince / HOUR;
    
    //Handle Plural
    if(hoursSince == 1)
        return @"1 hour ago";
    else
        return [NSString stringWithFormat:@"%d hours ago", hoursSince];
//    returns time "1:28 PM"
//    //Create date formatter
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    
//    //Format
//    [dateFormatter setDateFormat:@"h:mm a"];
//    return [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:self]];
}


// Yesterday = "Yesterday at 1:28 PM"
- (NSString *)formatAsYesterday
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"h:mm a"];
    return [NSString stringWithFormat:@"Yesterday at %@", [dateFormatter stringFromDate:self]];
}


// < Last 7 days = "Friday at 1:48 AM"
- (NSString *)formatAsLastWeek
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"EEEE 'at' h:mm a"];
    return [dateFormatter stringFromDate:self];
}


// < Last 30 days = "March 30 at 1:14 PM"
- (NSString *)formatAsLastMonth
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"MMMM d 'at' h:mm a"];
    return [dateFormatter stringFromDate:self];
}


// < 1 year = "September 15"
- (NSString *)formatAsLastYear
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"MMMM d"];
    return [dateFormatter stringFromDate:self];
}


// Anything else = "September 9, 2011"
- (NSString *)formatAsOther
{
    //Create date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    //Format
    [dateFormatter setDateFormat:@"LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}


/*
 =======================================================================
 */





#pragma mark - dateOperations

+(NSString *)stringFromDate:(NSDate *)aDate
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string=[formatter stringFromDate:aDate];
    return string;
}

+(NSDate *)datefromString:(NSString *)aString
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *aDate=[formatter dateFromString:aString];
    return aDate;
}

-(NSString *)timeStringfromDateTime
{
    
    NSDateFormatter *dateTimeFormatter1=[[NSDateFormatter alloc]init];
    [dateTimeFormatter1 setDateFormat:@"hh:mm a"];
    
    return [dateTimeFormatter1 stringFromDate:self];
}

-(NSString *)dateStringfromDateTime
{
    
    NSDateFormatter *dateTimeFormatter1=[[NSDateFormatter alloc]init];
    [dateTimeFormatter1 setDateFormat:@"LLL d, yyyy"];
    
    return [dateTimeFormatter1 stringFromDate:self];
}

-(NSString *)durationBetween:(NSDate *)fDate and:(NSDate *) sDate
{
    
    NSTimeInterval distanceBetweenDates = [sDate timeIntervalSinceDate:fDate];
    double secondsInAnHour = 3600;
    float hoursBetweenDates = (float)distanceBetweenDates / secondsInAnHour;
    return [NSString stringWithFormat:@"%.02f",hoursBetweenDates];
}

+(NSString *)timeStringFromDateTimeString:(NSString *)aString
{
    NSString *returnString = [[NSDate datefromString:aString] timeStringfromDateTime];
    return returnString;
}

+(NSString *)dateStringFromDateTimeString:(NSString *)aString
{
    
    NSString *returnString = [[NSDate datefromString:aString] dateStringfromDateTime];
    return returnString;
}

-(NSString *)dateFormatHeader
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"EEEE, dd MMMM yyyy"];
    NSString *aString = [formatter stringFromDate:self];
    
    return aString;
}

@end
