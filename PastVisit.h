//
//  PastVisit.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BDHPatientVisitDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface PastVisit : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (void)savePastVisit:(BDHPatientVisitDetails *)visitDetails;
+ (void)deleteAllVisitList;
+ (NSArray *)getAllVisitDetails;

@end

NS_ASSUME_NONNULL_END

#import "PastVisit+CoreDataProperties.h"
