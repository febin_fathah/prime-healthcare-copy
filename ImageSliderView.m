//
//  ImageSliderView.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/28/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "ImageSliderView.h"

@implementation ImageSliderView
@synthesize scrollView = _scrollView;

- (UIView *)hitTest:(CGPoint)point
          withEvent:(UIEvent *)event
{
    BOOL pointInside = [self pointInside:point
                               withEvent:event];
    
    if (pointInside && _scrollView)
    {// if point resides inside this view and UIScrollView object exists
        
        if (CGRectContainsPoint(_scrollView.frame, point))
        {// if point resides inside UIScrollView
            
            return [super hitTest:point withEvent:event]; // do not override
        }
        
        else
        {// if point resides outside UIScrollView
            
            return _scrollView; // override to return UIScrollView
        }
    }
    
    return [super hitTest:point withEvent:event]; // do not override
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
