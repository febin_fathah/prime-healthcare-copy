//
//  DBModels.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef DBModels_h
#define DBModels_h

#import "UserProfile.h"
#import "PastVisit.h"
#import "FutureAppointments.h"
#import "FavoriteDoctor.h"

#endif /* DBModels_h */
