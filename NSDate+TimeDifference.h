//
//  NSDate+TimeDifference.h
//  Magnet
//
//  Created by Mobile Developer on 21/09/15.
//  Copyright (c) 2015 Mobile Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TimeDifference)
/*
 Mysql Datetime Formatted As Time Ago
 Takes in a mysql datetime string and returns the Time Ago date format
 */
+ (NSString*)mysqlDatetimeFormattedAsTimeAgo:(NSString *)mysqlDatetime;


/*
 Formatted As Time Ago
 Returns the time formatted as Time Ago (in the style of Facebook's mobile date formatting)
 */
- (NSString *)formattedAsTimeAgo;

-(NSString *)dateFormatHeader;
+(NSString *)timeStringFromDateTimeString:(NSString *)aString;
+(NSString *)dateStringFromDateTimeString:(NSString *)aString;
+(NSString *)stringFromDate:(NSDate *)aDate;
+(NSDate *)datefromString:(NSString *)aString;
-(NSString *)timeStringfromDateTime;
-(NSString *)dateStringfromDateTime;
-(NSString *)durationBetween:(NSDate *)fDate and:(NSDate *) sDate;


- (BOOL)isSameDayAs:(NSDate *)comparisonDate;
- (NSString *)formattedForMiryde;


@end
