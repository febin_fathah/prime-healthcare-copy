//
//  ImageSliderView.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/28/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSliderView : UIView
{
    UIScrollView *_scrollView;
}
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@end
