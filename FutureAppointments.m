//
//  FutureAppointments.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FutureAppointments.h"
#import "AppDelegate.h"



#define kEntityName @"FutureAppointments"

@implementation FutureAppointments

// Insert code here to add functionality to your managed object subclass
+ (void)saveFutureAppointment:(EDRFutureAppointmentDetails *)details{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    FutureAppointments *appointment = [NSEntityDescription insertNewObjectForEntityForName:kEntityName inManagedObjectContext:context];
    appointment.appointmentDate = details.AppointmentDate;
    appointment.branchName = details.BranchName;
    appointment.doctorName = details.DoctorName;
    appointment.doctorPhoto = details.DoctorPhoto;
    appointment.qualification = details.Qualification;
    appointment.speciality = details.Speciality;
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save user Appoinment details! %@ %@", error, [error localizedDescription]);
    }
    else
    {
        NSLog(@"User user appointment details saved Successfully");
    }
}

+ (void)deleteAllFutureAppointmentsList{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSPersistentStoreCoordinator *myPersistentStoreCoordinator = [delegate persistentStoreCoordinator];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kEntityName];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *deleteError = nil;
    if ([myPersistentStoreCoordinator executeRequest:delete withContext:myContext error:&deleteError]) {
        NSLog(@"Deleted appointment details Successfully");
    }
    else{
        NSLog(@"Can't delete appointment details! %@! %@",deleteError, deleteError.localizedDescription);
        
    }
}

+ (NSArray *)getAllFutureAppointmentsDetails{
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    //    [request setFetchLimit:1];
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    
    
    if (error != nil) {
        
        NSLog(@"Failed to get appointment details! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
    }
    return results;
}

@end
