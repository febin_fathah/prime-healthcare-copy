//
//  PastVisit.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "PastVisit.h"
#import "AppDelegate.h"

#define kEntityName @"PastVisit"

@implementation PastVisit

// Insert code here to add functionality to your managed object subclass

+ (void)savePastVisit:(BDHPatientVisitDetails *)visitDetails{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    PastVisit *pastVisit = [NSEntityDescription insertNewObjectForEntityForName:kEntityName inManagedObjectContext:context];
    pastVisit.branchCode = visitDetails.BranchCode;
    pastVisit.branchId = [NSString stringWithFormat:@"%@",visitDetails.BranchId];
    pastVisit.branchName = visitDetails.BranchName;
    pastVisit.doctorId = visitDetails.DoctorId;
    pastVisit.doctorName = visitDetails.DoctorName;
    pastVisit.doctorPhoto = visitDetails.DoctorPhoto;
    pastVisit.qualification = visitDetails.Qualification;
    pastVisit.speciality = visitDetails.Speciality;
    pastVisit.visitDate = visitDetails.VisitDate;
    pastVisit.visitId = [NSString stringWithFormat:@"%@",visitDetails.VisitId];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save user vistDetails! %@ %@", error, [error localizedDescription]);
    }
    else
    {
        NSLog(@"User user visit details saved Successfully");
    }
}

+ (void)deleteAllVisitList{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSPersistentStoreCoordinator *myPersistentStoreCoordinator = [delegate persistentStoreCoordinator];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kEntityName];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *deleteError = nil;
    if ([myPersistentStoreCoordinator executeRequest:delete withContext:myContext error:&deleteError]) {
        NSLog(@"Deleted Visitdetails Successfully");
    }
    else{
        NSLog(@"Can't delete visitDetails! %@! %@",deleteError, deleteError.localizedDescription);
        
    }
}

+ (NSArray *)getAllVisitDetails{
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
//    [request setFetchLimit:1];
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    
    
    if (error != nil) {
        
        NSLog(@"Failed to get Visti details! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
    }
    
    return results;
}
    @end

