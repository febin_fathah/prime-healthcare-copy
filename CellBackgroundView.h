//
//  CellBackgroundView.h
//
//

#import <UIKit/UIKit.h>

typedef enum {
    CellPositionTop,
    CellPositionMiddle,
    CellPositionBottom,
    CellPositionSingle
} CellPosition;

@interface CellBackgroundView : UIView

@property(assign) CellPosition position;
@property(strong) UIColor *fillColor;
@property(strong) UIColor *borderColor;

@end
