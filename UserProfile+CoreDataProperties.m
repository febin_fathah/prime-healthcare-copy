//
//  UserProfile+CoreDataProperties.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserProfile+CoreDataProperties.h"

@implementation UserProfile (CoreDataProperties)

@dynamic dateOfBirth;
@dynamic email;
@dynamic emiratesID;
@dynamic isChronic;
@dynamic isLoyal;
@dynamic mobileNo;
@dynamic patientName;
@dynamic regNo;

@end
