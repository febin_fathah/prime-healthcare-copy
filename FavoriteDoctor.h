//
//  FavoriteDoctor.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CMEDoctorDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavoriteDoctor : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (void)saveDoctor:(CMEDoctorDetails *)details;
+ (void)deleteAllDoctors;
+ (NSArray *)getAllDoctors;
+ (NSArray *)getAllFavoriteDoctors;
+ (BOOL)updateDoctor;
+ (void)addFavoriteDoctor:(NSString *)doctorID add:(BOOL)add;
+ (FavoriteDoctor *)getDoctor:(NSString *)doctorID;

@end

NS_ASSUME_NONNULL_END

#import "FavoriteDoctor+CoreDataProperties.h"
