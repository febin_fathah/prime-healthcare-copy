//
//  FavoriteDoctor+CoreDataProperties.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FavoriteDoctor.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavoriteDoctor (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *doctorName;
@property (nullable, nonatomic, retain) NSString *doctorId;
@property (nullable, nonatomic, retain) NSString *regNumber;
@property (nullable, nonatomic, retain) NSString *clinicId;
@property (nullable, nonatomic, retain) NSString *clinicName;
@property (nullable, nonatomic, retain) NSString *departmentId;
@property (nullable, nonatomic, retain) NSString *discriptionText;
@property (nullable, nonatomic, retain) NSString *doctorPhoto;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *qualification;
@property (nullable, nonatomic, retain) NSNumber *favoriteAdded;
@property (nullable, nonatomic, retain) NSString *departmentName;

@end

NS_ASSUME_NONNULL_END
