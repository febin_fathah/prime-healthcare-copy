//
//  LoggedUserViewController.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/30/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "USLUserProfile.h"
@class LoggedUserViewController;

@protocol LoggedUserViewControllerDelegate<NSObject>

@optional
-(void)recivedOTP:(NSString *)otp withRegisterNumber:(NSString *)regNo fromLoggedUserViewController:(LoggedUserViewController *)vc;
- (void)didClickSkipButton;
- (void)loginCompletedSuccessfully;

@end
@interface LoggedUserViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *submitBTN;
@property (weak, nonatomic) IBOutlet UIImageView *profImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextFeild;
@property (weak, nonatomic) IBOutlet UIView *baseView;


@property(weak,nonatomic) NSDictionary *currentUser;

@property (assign, nonatomic) id <LoggedUserViewControllerDelegate>delegate;

@end
